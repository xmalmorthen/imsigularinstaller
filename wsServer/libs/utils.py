import time
import datetime
import os
import signal
import psutil

def getTimeStamp():
  ts = time.time()
  timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
  return timestamp

def get_pid(process_name):
    pid = []
    for proc in psutil.process_iter():
        for pn in process_name:
            if proc.name().find(pn) != -1:
                pid.append(proc.pid)
    return pid

def kill_pid(pids):
  if len(pids) > 0:
    for pid in pids:
      os.kill(pid,signal.SIGKILL)
