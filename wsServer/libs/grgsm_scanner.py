import shlex
import signal
import time
import os
import sys
import subprocess
from threading import Timer
import json
from libs import utils
import globals

class grgsm_scannerClass:

    def __init__(self, gain, ppm, speed = 4, residualFileName = 'grgsm_scanner_out.txt', testMode = False):
        self.gain = gain
        self.ppm = ppm
        self.speed = speed
        self.residualFileName = residualFileName
        self.testMode = testMode
        self.cmd = 'sudo grgsm_scanner -b PCS1900 -s 2e6 -p {0} -g {1} --args=RTL=0 --speed={2} -v -d'.format(self.ppm, self.gain,self.speed)

        #print ('prepare grgsm_scanner')
        #print(self.cmd)

        self.outputData = ''
        self.outputModel = None

    def __testMode(self):
        residualFilePath = '{0}/{1}'.format(globals.FULLRESIDUALFILESPATH,'grgsm_scanner_out_testmode.txt')
        outputFile = open(residualFilePath, 'r')
        data = outputFile.read()
        outputFile.close()
        return data

    def procIsRunning(self, id):
        response = None

        qry = "SELECT jsonData FROM `main`.`servicestatus` WHERE id = {0} and jsonData is not NULL".format(id)

        iter = 10
        while True:
            qryResponse = globals.DB.run_query(qry)
            if len(qryResponse) != 0:
                response = qryResponse[0][0]
                break

            time.sleep(60)

            iter -= 1

            if (iter == 0):
                utils.kill_pid(utils.get_pid(['grgsm_scanner']))

                qry = "UPDATE `main`.`servicestatus` SET 'status'='-1', `fAct` = '{0}' WHERE `id` = {1}".format(utils.getTimeStamp(),id)
                globals.DB.run_query(qry)
                self.do()
                response = json.dumps( self.outputModel )
                break

        return response

    def do(self):

        service = 'grgsm_scanner'
        qry = "SELECT id FROM `main`.`servicestatus` WHERE service = '{0}' and status = '1' ORDER by fIns DESC".format(service)
        response = globals.DB.run_query(qry)

        from actions import loopBackAction
        loopBackAction.RTLCalibrate().kill()

        if len(response) != 0:

            self.outputModel = json.loads(self.procIsRunning(response[0][0]))
            return

        else:

            qry = "INSERT INTO `main`.`servicestatus`(`service`) VALUES ('{0}')".format(service)
            globals.DB.run_query(qry)
            id = globals.DB.lastrowid

            residualFilePath = '{0}/{1}'.format(globals.FULLRESIDUALFILESPATH,self.residualFileName)

            outputFile = open(residualFilePath, 'w')

            if (self.testMode):
                outputFile.write(self.__testMode())
            else:
                outputFile.write('\n')

            outputFile.flush()

            if (not self.testMode):
                p = subprocess.Popen(shlex.split(self.cmd),
                        shell=False,
                        stdout=outputFile,
                        stderr=outputFile)

                p.communicate()

            outputFile.close()

            time.sleep(1)

            arfcns = []

            if os.path.exists(residualFilePath):
                residualFile = open(residualFilePath, "r")

                idArfcn = 1
                for line in residualFile:
                    if line.find('ARFCN:') != -1:
                        arfcn = { 'id' : idArfcn}
                        parser = line.split(",")
                        for iter in parser:
                            dataParts = iter.split(":")
                            arfcn[dataParts[0].strip().replace("O", "")] = dataParts[1].strip()

                        if (
                            arfcn['MCC'] != 0
                            and arfcn['MNC'] != 0
                            and arfcn['LAC'] != 0
                            and arfcn['CID'] != 0
                        ):

                          qry = "SELECT lon,lat FROM 'main'.'bts' WHERE mcc={0} and mnc={1} and lac={2} and cid={3} LIMIT 1".format(arfcn['MCC'],arfcn['MNC'],arfcn['LAC'],arfcn['CID'])

                          response = globals.BTSDB.run_query(qry)

                          if len(response) != 0:
                            arfcn['cords'] = {
                              'lat': response[0][1],
                              'lng': response[0][0]
                            }

                          #TODO: Only test
                          '''
                          arfcn['cords'] = {
                            'lat': 19.252329865691525,
                            'lng': -103.70808832910251
                          }
                          '''

                          arfcn['config'] = {
                            'gain': self.gain,
                            'ppm': self.ppm,
                            'speed': self.speed
                          }

                        arfcns.append(arfcn)
                        if ( int(arfcn['MNC']) == 20):
                            idArfcn += 1

                    if line.find('Configuration:') != -1:
                        lastDict = arfcns[-1]
                        lastDict['Configuration'] = line.split(':')[1].strip()
                    if line.find('Cell ARFCNs:') != -1:
                        lastDict = arfcns[-1]
                        lastDict['ARFCNs'] = line.split(':')[1].split(',')
                    if line.find('Neighbour Cells:') != -1:
                        lastDict = arfcns[-1]
                        lastDict['Neighbour_Cells'] = line.split(':')[1].split(',')
                residualFile.close()
                os.remove(residualFilePath)
            else:
                raise Exception('residual file: [ {0} ] not found...'.format(residualFilePath))

            self.outputModel = arfcns

            jsonData = json.dumps(self.outputModel)
            timestamp = utils.getTimeStamp()

            qry = "UPDATE `main`.`servicestatus` SET 'status'='0', `jsonData`= '{0}', `fAct` = '{1}' WHERE `id` = {2}".format(jsonData,timestamp,id)
            globals.DB.run_query(qry)

            #print (self.outputModel)
            #print ('grgsm_scanner end')

    def kill(self):
        service = 'grgsm_scannerKill'
        qry = "SELECT id FROM `main`.`servicestatus` WHERE service = '{0}' and status = '1' ORDER by fIns DESC".format(service)
        response = globals.DB.run_query(qry)

        if len(response) != 0:
          return
        else:

          qry = "INSERT INTO `main`.`servicestatus`(`service`) VALUES ('{0}')".format(service)
          globals.DB.run_query(qry)
          id = globals.DB.lastrowid

          utils.kill_pid(utils.get_pid(['grgsm_scanner','rtl_test']))

          qry = "UPDATE `main`.`servicestatus` SET `status` = '-1' WHERE `service` IN ('rtl_test','grgsm_scanner') AND `status` = '1'"
          globals.DB.run_query(qry)

          timestamp = utils.getTimeStamp()

          qry = "UPDATE `main`.`servicestatus` SET 'status'='0', `fAct` = '{0}' WHERE `id` = {1}".format(timestamp,id)
          globals.DB.run_query(qry)

    def __str__(self):
        return json.dumps(self.outputModel) if self.outputModel != None else None
