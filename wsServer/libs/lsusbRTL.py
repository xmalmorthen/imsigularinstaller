import shlex
import subprocess
from threading import Timer

class lsusbRTLClass:
    
    def __init__(self):
        self.cmd = 'sudo lsusb'
        
    def do(self):
        child = subprocess.Popen(
                shlex.split(self.cmd),
                shell=False, 
                stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE
            )
        
        out, err = child.communicate()
        outputData = out.decode() + err.decode()

        for line in outputData.splitlines():            
            if (line.find('Realtek Semiconductor') != -1):
                return True
        
        return False