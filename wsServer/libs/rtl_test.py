import shlex
import signal
import time
import os
import sys
import subprocess
from threading import Timer
import json
from libs import utils
import globals

class rtl_testClass:

    def __init__(self, backMode = None):
        self.cmd = 'rtl_test -s 2e6 -d 0 -t -p 10'
        self.backMode = backMode
        self.outputData = ''
        self.outputModel = None

        #print ('prepare rtl_test')
        #print(self.cmd)

    def procIsRunning(self, id):
        response = None

        qry = "SELECT jsonData FROM `main`.`servicestatus` WHERE id = {0} and jsonData is not NULL".format(id)

        iter = 16
        while True:
            qryResponse = globals.DB.run_query(qry)
            if len(qryResponse) != 0:
                response = qryResponse[0][0]
                break

            time.sleep(5)

            iter -= 1

            if (iter == 0):
                utils.kill_pid(utils.get_pid(['rtl_test']))

                qry = "UPDATE `main`.`servicestatus` SET 'status'='-1', `fAct` = '{0}' WHERE `id` = {1}".format(utils.getTimeStamp(),id)
                globals.DB.run_query(qry)
                self.do()
                response = json.dumps( self.outputModel )
                break

        return response

    def do(self):

        service = 'rtl_test'
        id = None

        qry = "SELECT id FROM `main`.`servicestatus` WHERE service = '{0}' and status = '1' ORDER by fIns DESC".format(service)
        response = globals.DB.run_query(qry) if not self.backMode else []

        if not self.backMode:
          from actions import loopBackAction
          loopBackAction.RTLCalibrate().kill()

        if len(response) != 0:

          self.outputModel = json.loads(self.procIsRunning(response[0][0]))
          return

        else:

          qry = "INSERT INTO `main`.`servicestatus`(`service`) VALUES ('{0}')".format(service)
          if not self.backMode:
            globals.DB.run_query(qry)
          id = globals.DB.lastrowid if not self.backMode else None

          cmd = './scripts/usbfs_memory_mb.sh'

          child = subprocess.Popen(
                  shlex.split(cmd),
                  shell=True,
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE
              )
          child.communicate()

          child = subprocess.Popen(
                  shlex.split(self.cmd),
                  shell=False,
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE
              )

          tm = Timer(60, child.send_signal, [signal.SIGINT])
          tm.start()
          out, err = child.communicate()

          self.outputData = out.decode() + err.decode()

        gainValues = []
        cumulativeValue = None

        cumulativeIter = 0
        try:
            for line in self.outputData.splitlines():
                if (line.find('Failed to open rtlsdr device') != -1):
                    raise Exception('Failed to open')

                if (line.find('Supported gain values') != -1):
                    parser = line.split(":")
                    gainValues = parser[1].split()
                if (line.find('cumulative PPM') != -1):
                    if cumulativeIter == 2:
                        parser = line.split("cumulative PPM:")
                        cumulativeValue = int(parser[1].strip())
                    else:
                        cumulativeIter += 1

            jsonData = json.dumps({"gainValues" : gainValues,"PPM": cumulativeValue})
            self.__updateStatusService(jsonData,id)

            if (len(gainValues) > 0):
                if (cumulativeValue != None):
                    self.outputModel = {
                        "status": True,
                        "gainValues" : gainValues,
                        "PPM": cumulativeValue
                    }
                else:
                    raise Exception('RTL device error, not possible read cumulativa PPM')
            else:
                raise Exception('RTL device not found...')
        except Exception as ex:
            tm.cancel()

            self.__updateStatusService(None,id)

            if (str(ex).find('Failed to open') != -1 ):
                from libs.resetRTLDevice import usbresetClass
                usbreset = usbresetClass()

                try:
                    usbreset.do()
                    self.do()
                except Exception as err:
                    raise err

            else:
                raise ex

        #print (self.outputModel)
        #print ('rtl_test end')

    def __updateStatusService(self,jsonData,id):
        if id and not self.backMode:
          timestamp = utils.getTimeStamp()
          qry = "UPDATE `main`.`servicestatus` SET 'status'='0', `jsonData`= '{0}', `fAct` = '{1}' WHERE `id` = {2}".format(jsonData,timestamp,id)
          globals.DB.run_query(qry)

    def __str__(self):
        return json.dumps(self.outputModel) if self.outputModel != None else None

class rtl_testGainOnlyClass:

    def __init__(self):
        self.cmd = 'rtl_test -s 2e6 -d 0 -t -p 10'
        self.outputData = ''
        self.outputModel = None

    def procIsRunning(self, id):
        response = None

        qry = "SELECT jsonData FROM `main`.`servicestatus` WHERE id = {0} and jsonData is not NULL".format(id)

        iter = 2
        while True:
            qryResponse = globals.DB.run_query(qry)
            if len(qryResponse) != 0:
                response = qryResponse[0][0]
                break

            time.sleep(5)

            iter -= 1

            if (iter == 0):
                utils.kill_pid(utils.get_pid(['rtl_test']))

                qry = "UPDATE `main`.`servicestatus` SET 'status'='-1', `fAct` = '{0}' WHERE `id` = {1}".format(utils.getTimeStamp(),id)
                globals.DB.run_query(qry)
                self.do()
                response = json.dumps( self.outputModel )
                break

        return response

    def do(self):

        service = 'rtl_testGainOnly'

        qry = "SELECT id FROM `main`.`servicestatus` WHERE service = '{0}' and status = '1' ORDER by fIns DESC".format(service)
        response = globals.DB.run_query(qry)

        if len(response) != 0:

          self.outputModel = json.loads(self.procIsRunning(response[0][0]))
          return

        else:

          qry = "INSERT INTO `main`.`servicestatus`(`service`) VALUES ('{0}')".format(service)
          globals.DB.run_query(qry)
          id = globals.DB.lastrowid

          cmd = './scripts/usbfs_memory_mb.sh'

          child = subprocess.Popen(
                  shlex.split(cmd),
                  shell=True,
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE
              )
          child.communicate()

          child = subprocess.Popen(
                  shlex.split(self.cmd),
                  shell=False,
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE
              )

          tm = Timer(3, child.send_signal, [signal.SIGINT])
          tm.start()
          out, err = child.communicate()

          self.outputData = out.decode() + err.decode()

        gainValues = []

        try:
            for line in self.outputData.splitlines():
                if (line.find('Failed to open rtlsdr device') != -1):
                    raise Exception('Failed to open')

                if (line.find('Supported gain values') != -1):
                    parser = line.split(":")
                    gainValues = parser[1].split()

            self.outputModel = {
                "status": True,
                "gainValues" : gainValues
            }

            jsonData = json.dumps(self.outputModel)
            timestamp = utils.getTimeStamp()

            qry = "UPDATE `main`.`servicestatus` SET 'status'='0', `jsonData`= '{0}', `fAct` = '{1}' WHERE `id` = {2}".format(jsonData,timestamp,id)
            globals.DB.run_query(qry)

            if (len(gainValues) == 0):
                raise Exception('RTL device not found...')

        except Exception as ex:
            try:
                tm.cancel()
            except Exception as err:
                pass

            if (str(ex).find('Failed to open') != -1 ):
                from libs.resetRTLDevice import usbresetClass
                usbreset = usbresetClass()

                try:
                    usbreset.do()
                    self.do()
                except Exception as err:
                    raise err

            else:
                raise ex

    def __str__(self):
        return json.dumps(self.outputModel) if self.outputModel != None else None
