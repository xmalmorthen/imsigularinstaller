import globals as globals
from libs import utils
from libs.resetRTLDevice import usbresetClass
from libs.loggerLib import Logger
import inspect

class resetClass:

    def __init__(self):
        pass

    def do(self):
        Logger.set('debug', f'resetClass.{inspect.stack()[0][3]} | init' )

        result = None

        globals.GLOBALMSG = 'tsharkKill'
        globals.RESSETING = True

        try:

          servicesList = ['tshark','grgsm_livemon','grgsm_scanner','rtl_test']

          Logger.set('debug', f'resetClass.{inspect.stack()[0][3]} => resseting services [ {servicesList} ]' )

          utils.kill_pid(utils.get_pid(servicesList))

          Logger.set('debug', f'resetClass.{inspect.stack()[0][3]} => services resseted  [ {servicesList} ]' )

          usbreset = usbresetClass()
          usbreset.do()

          qry = "UPDATE `main`.`servicestatus` SET 'status'='-1', `fAct` = '{0}' WHERE `status` = 1".format(utils.getTimeStamp())
          globals.DB.run_query(qry)

          globals.RESSETING = False

          result = {
            'status': True,
            'message': 'system has been reset'
          }


        except Exception as err:
          result = {
            'status': False,
            'message': str(err)
          }

        Logger.set('debug', f'resetClass.{inspect.stack()[0][3]} => result [ {result} ]' )

        Logger.set('debug', f'resetClass.{inspect.stack()[0][3]} | end' )

        return result

