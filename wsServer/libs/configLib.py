# -*- coding: utf-8 -*-

import configparser
import sys
import os
import globals as globals

class config:
    def __init__(self):
        try:
          
          if not os.path.exists(globals.CONFIGFILENAME):
            raise Exception('config file required')

          self.config = configparser.ConfigParser()
          self.config.read(globals.CONFIGFILENAME)

        except Exception as err:
          raise err

    def get(self, section, value):
        response = None
        try:
            response = self.config[section][value]
        except getopt.GetoptError as err:
            response = None
        return response
