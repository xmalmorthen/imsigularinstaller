import datetime
import globals
import jwt

class AuthClass:

  def __init__(self):
    self.key = 'lqaBiYXygtfzc2t6KgPUMDH6lZ8qXu57'

  @staticmethod
  def admCodes():
    return [
      '..121212qw',
      '10101011',
      '1ms1'
    ]

  @staticmethod
  def addClient(sid):
    timeLimit= datetime.datetime.utcnow() + datetime.timedelta(days= 1)
    payload = {"sid": sid,"exp":timeLimit}
    jwToken = jwt.encode(payload,AuthClass().key,algorithm='HS256')

    globals.SOCKETCLIENTS.append({'sid': sid, 'jwToken': jwToken})

    return jwToken


  @staticmethod
  def checkIfAuth(sid):
    response = {'authenticated': False, 'message': 'Not authenticated'}

    data = None
    try:
      data = next(item for item in globals.SOCKETCLIENTS if item["sid"] == sid)
    except Exception:
      pass

    if data:
      try:
        AuthClass.validateToken(data.jwToken)
        response = { 'authenticated': True }
      except Exception as ex:
        response = { 'authenticated': False, 'message': ex }

    return response

  @staticmethod
  def validateToken(jwToken):
    try:
      return jwt.decode(jwToken,AuthClass().key, algorithms=['HS256'])
    except jwt.exceptions.ExpiredSignatureError:
      raise Exception('Token has expired')
    except Exception:
      raise Exception('Invalid Token')
