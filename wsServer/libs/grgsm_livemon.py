import shlex
import signal
import threading
from threading import Timer
import os
import time
import json
from subprocess import Popen, PIPE
import globals as globals
from libs import utils
from random import seed
from random import randint
from datetime import datetime
import math
from libs.loggerLib import Logger
import inspect

class grgsm_livemon_nogui(threading.Thread):
    def __init__(self,freq,gain,ppm, testMode):
        self.response = None

        self.freq = freq
        self.gain = gain
        self.ppm = ppm

        self.testMode = testMode

        '''
        self.cmd = 'sudo grgsm_livemon_headless --args=RTL=0 -f {0} -g {1} -p {2} -s 2e6'.format(
                      self.freq,
                      self.gain,
                      self.ppm
                    )
        '''

        self.cmd = 'sudo grgsm_livemon --args=RTL=0 -f {0} -g {1} -p {2} -s 2e6'.format(
                      self.freq,
                      self.gain,
                      self.ppm
                    )

        #print ('prepare grgsm_livemon')
        #print (self.cmd)

        threading.Thread.__init__(self)

    def run(self):
        try:

          from actions import loopBackAction
          loopBackAction.RTLCalibrate().kill()

          if (self.testMode):
              return

          child = Popen(shlex.split(self.cmd),shell=False, stdin=PIPE, stdout=PIPE, stderr=PIPE)
          out, err = child.communicate()

          self.response = out.decode("utf-8") + err.decode("utf-8")

        except Exception as err:
            self.response = { "status": False, "message": str(err)}

    def kill(self):
        service = 'grgsm_livemonKill'
        qry = "SELECT id FROM `main`.`servicestatus` WHERE service = '{0}' and status = '1' ORDER by fIns DESC".format(service)
        response = globals.DB.run_query(qry)

        if len(response) != 0:
          return
        else:

          qry = "INSERT INTO `main`.`servicestatus`(`service`) VALUES ('{0}')".format(service)
          globals.DB.run_query(qry)
          id = globals.DB.lastrowid

          globals.GLOBALMSG = 'tsharkKill'

          utils.kill_pid(utils.get_pid(['tshark','grgsm_livemon']))
          time.sleep(1)
          utils.kill_pid(utils.get_pid(['tshark','grgsm_livemon']))
          time.sleep(1)
          utils.kill_pid(utils.get_pid(['tshark','grgsm_livemon']))

          qry = "UPDATE `main`.`servicestatus` SET `status` = '-1' WHERE `service` IN ('grgsm_livemon') AND `status` = '1'"
          globals.DB.run_query(qry)

          timestamp = utils.getTimeStamp()

          qry = "UPDATE `main`.`servicestatus` SET 'status'='0', `fAct` = '{0}' WHERE `id` = {1}".format(timestamp,id)
          globals.DB.run_query(qry)

class tsharkParser(threading.Thread):
    def __init__(self, residualFileName, servideId, freq, gain, ppm, btsInfo, testMode):
        self.response = None
        self.residualFileName = residualFileName
        self.servideId = servideId
        self.freq = freq
        self.gain = gain
        self.ppm = ppm
        self.btsInfo = btsInfo
        self.testMode = testMode

        self.tsharkCPACFile = '/root/tsharkScann.cpac' if self.testMode != True else '{0}/{1}'.format(globals.FULLRESIDUALFILESPATH,'tsharkScann_out_testmode.cpac')

        self.cmdTshark = "sudo -i tshark -i lo -w {0} -f 'udp port 4729' -a duration:60".format(self.tsharkCPACFile)
        self.cmdTsharkFormat = "sudo -i tshark -r {0} -T jsonraw".format(self.tsharkCPACFile)

        globals.GLOBALMSG = None

        seed(1)

        #print ('prepare tshark')

        #print (self.cmdTshark)
        #print (self.cmdTsharkFormat)

        #print (self.servideId,self.freq,self.gain,self.ppm)

        #print (self.btsInfo)


        threading.Thread.__init__(self)

    def run(self):
        while True:

          self.response = {}

          try:

              Logger.set('debug',f'tsharkParser.{inspect.stack()[0][3]} => tshark | init' )

              if globals.GLOBALMSG == 'tsharkKill':
                  #globals.GLOBALMSG = None
                  Logger.set('debug',f'tsharkParser.{inspect.stack()[0][3]} => tshark kill' )
                  break

              jsonData = None
              response = {}

              qry = "INSERT INTO `main`.`scanProcTbl`(`freq`,`gain`,`ppm`,`idServiceStatus`) VALUES ('{0}','{1}','{2}',{3})".format(
                self.freq,
                self.gain,
                self.ppm,
                self.servideId
              )

              id = -1

              if (self.testMode != True):
                  globals.DB.run_query(qry)
                  id = globals.DB.lastrowid

                  child = Popen(shlex.split(self.cmdTshark),shell=False, stdin=PIPE, stdout=PIPE, stderr=PIPE)
                  child.communicate()

              outputFile = open('{0}.json'.format(self.residualFileName), 'w')
              outputFile.write('')
              outputFile.flush()

              child = Popen(shlex.split(self.cmdTsharkFormat),shell=False,
                            stdin=PIPE,
                            stdout=outputFile,
                            stderr=outputFile)
              child.communicate()

              outputFile.close()

              time.sleep(1)

              if os.path.exists('{0}.json'.format(self.residualFileName)):

                  if globals.GLOBALMSG != 'tsharkKill':

                      #print ('file found')

                      linesCount = sum(1 for line in open('{0}.json'.format(self.residualFileName)))

                      #print ('cantidad de líneas: {0}'.format(linesCount))

                      if (linesCount <= 10):
                          response = {
                              "status": False,
                              'statusCode': 1 if globals.GLOBALMSG != 'tsharkKill' else 0,
                              "message": 'Data could not be read' if globals.GLOBALMSG != 'tsharkKill' else "target's scan cancelled"
                          }

                      else:

                          arrModel = []

                          json_file = None
                          with open('{0}.json'.format(self.residualFileName)) as f:
                            json_file = f.readlines()[2:]

                          with open('{0}.json'.format(self.residualFileName),'w') as f:
                            for item in json_file:
                              f.write("%s\n" % item)

                          jsonData = []
                          try:
                            with open('{0}.json'.format(self.residualFileName), 'r') as myfile:
                              data=myfile.read()
                              jsonData = json.loads(data)
                          except Exception as ex:
                            #print ('tsharkParser ','Error al convertir json ', ex)
                            pass

                          iter = 1

                          for root in jsonData:

                            item = {
                              'id': iter,
                              'date': datetime.strftime( datetime.now(), "%d-%m-%Y" ),
                              'time': datetime.strftime( datetime.now(), "%H:%M:%S"),
                              'frame': '',
                              'arfcn': '',
                              'imsi': '',
                              'tmsi': '',
                              'signal': '',
                              'snr': '',
                              'mcc': self.btsInfo['MCC'],
                              'mnc': self.btsInfo['MNC'],
                              'lac': self.btsInfo['LAC'],
                              'cid': self.btsInfo['CID'],
                              'dist': '',
                              'cords': '',
                              'antenna': ''
                            }

                            try:
                              layer = root['_source']['layers']

                              cjsonCad = json.dumps(layer)
                              if cjsonCad.find('IMSI') > 0:
                                item['imsi'] = (cjsonCad.split('IMSI (')[1].split(')')[0]).strip()

                                item['arfcn'] = int( layer['gsmtap']['gsmtap.arfcn_raw'][0] , 16)
                                item['signal'] = ( int("FF",16) - int(layer['gsmtap']['gsmtap.signal_dbm_raw'][0],16) ) * -1  #int( layer['gsmtap']['gsmtap.signal_dbm_raw'][0] , 16)
                                item['snr'] = int( layer['gsmtap']['gsmtap.snr_db_raw'][0] , 16)
                                item['frame'] = int( layer['gsmtap']['gsmtap.frame_nr_raw'][0] , 16)
                                item['antenna'] = int( layer['gsmtap']['gsmtap.antenna_raw'][0] , 16)
                                item['dist'] = "{0} m".format(self.calculateDistance(item))

                                arrModel.append(item)

                                iter += 1

                            except Exception as ex:
                              pass

                          self.response = arrModel

                          os.chmod('{0}'.format(self.tsharkCPACFile), 0o777)
                          os.chmod('{0}.json'.format(self.residualFileName), 0o777)

                          if (self.testMode != True):
                            os.remove('{0}'.format(self.tsharkCPACFile))
                          os.remove('{0}.json'.format(self.residualFileName))

                          response = {
                              "status": True if globals.GLOBALMSG != 'tsharkKill' else False,
                              "message": "tshark scanned" if globals.GLOBALMSG != 'tsharkKill' else "target's scan cancelled",
                              "data": self.response
                          }

                  else: #globals.GLOBALMSG != 'tsharkKill'

                      response = {
                          "status": False,
                          'statusCode': 0,
                          "message": "target's scan cancelled",
                      }
                      break

              else: #os.path.exists

                  #print ('not file found')

                  response = {
                      "status": False,
                      'statusCode': 2 if globals.GLOBALMSG != 'tsharkKill' else 0,
                      "message": 'file [ {0}.json ] of thsark parse not found'.format(self.residualFileName) if globals.GLOBALMSG != 'tsharkKill' else "target's scan cancelled"
                  }

                  if globals.GLOBALMSG == 'tsharkKill':
                    break

              jsonData = json.dumps(response)

              timestamp = utils.getTimeStamp()
              qry = "UPDATE `main`.`scanProcTbl` SET 'status'='0', `jsonData`= '{0}', `fAct` = '{1}' WHERE `id` = {2}".format(
                jsonData,
                timestamp,
                id
              )
              if (self.testMode != True):
                globals.DB.run_query(qry)

              globals.SOCKETIO.emit('tshark', response,namespace="/imsi", broadcast=True)

              Logger.set('debug', f'tsharkParser.{inspect.stack()[0][3]} => emit.tshark [ {response} ]' )

              if (self.testMode == True):
                  time.sleep(5)

          except Exception as err:
              self.response = { "status": False, "message": str(err)}

              msg = f'tsharkParser.{inspect.stack()[0][3]} => [ {ex} ]'
              #print(msg)
              Logger.set('error',msg)

          #print ('tshark run end')
          Logger.set('debug',f'tsharkParser.{inspect.stack()[0][3]} | end' )

    def calculateDistance (self, item):
      Pt = 2
      Pr = item['signal']
      Gt = 33
      Gr = 15
      f  = ( ( int(item['arfcn']) - 512 ) * 0.2 ) + 1850.2

      #d = 10 * ( ( Pt - Pr + Gt + Gr - (20 * ( math.log(f) ) ) - ( 20 * ( math.log( 30000000 / ( 4 * ( math.pi ) ) ) ) ) - 20 )  / 20 )

      d = Pt - Pr + Gt + Gr

      fLog = math.log(f,10)

      pi4 = 4 * math.pi
      lightPi = 30000000 / pi4

      piLightLog = math.log( lightPi , 10 )

      d = d - ( 20 * ( fLog ) ) - ( 20 * (piLightLog) ) - 20

      d = (d / 20) * 10

      d= round(abs(d),3)

      return d

