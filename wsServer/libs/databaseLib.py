# -*- coding: utf-8 -*-

import sys
import sqlite3
import os

class database:
    """Database connection class."""

    def __init__(self, config):
        self.config = config
        self.cnnString = '{0}/database/{1}'.format(os.getcwd(),self.config["dbname"])
        self.conn = None
        self.lastrowid = None

    def open_connection(self):
        """Connect to MySQL Database."""
        try:
            if self.conn is None:
                self.conn = sqlite3.connect(self.cnnString)
            return True
        except sqlite3.DatabaseError as e:
            return False

    def run_query(self, query):
        """Execute SQL query."""
        try:
            if not self.open_connection():
                raise sqlite3.DatabaseError("Not connection to db")

            cur = self.conn.cursor()

            if 'SELECT' in query:
                records = []
                cur.execute(query)
                result = cur.fetchall()
                for row in result:
                    records.append(row)
                cur.close()
                return records
            else:
                result = cur.execute(query)
                self.conn.commit()
                affected = "{0} rows affected.".format(cur.rowcount)
                self.lastrowid = cur.lastrowid
                cur.close()
                return affected
        except sqlite3.Error as e:
            raise e
        finally:
            if self.conn:
                self.conn.close()
                self.conn = None
