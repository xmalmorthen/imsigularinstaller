# -*- coding: utf-8 -*-

import logging
import globals
import datetime
import os
import inspect

class Logger:

  def __init__(self):
    logger = logging.getLogger(globals.CNFG['daemons']['server'])
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - [ %(levelname)s ] => [ %(message)s ]")
    logPath = "{0}/{1}_{2}.log".format(globals.LOGSPATH, globals.CNFG['daemons']['server'], datetime.datetime.now().strftime("%d_%m_%Y"))

    handler = logging.FileHandler(logPath)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    globals.SUPERLOGGER = logger


  @staticmethod
  def set(level,  message):
    if level == 'debug':
      globals.SUPERLOGGER.debug(message)
    elif level == 'info':
      globals.SUPERLOGGER.info(message)
    elif level == 'warning':
      globals.SUPERLOGGER.warning(message)
    elif level == 'error':
      globals.SUPERLOGGER.error(message)
    elif level == 'critical':
      globals.SUPERLOGGER.critical(message)
    else:
      globals.SUPERLOGGER.debug(message)

  @staticmethod
  def get():
    fileName = f'{globals.CNFG["daemons"]["server"]}_{datetime.datetime.now().strftime("%d_%m_%Y")}.log'
    logPath = f'{globals.LOGSPATH}/{fileName}'

    response = {}

    if not os.path.exists(logPath):
      print('not file')
      Logger.set('error',f'Logger.{inspect.stack()[0][3]} => Log file not exists [ {logPath} ]')

      response = {
        "status" : False,
        'message': f'Log file not exists [ {logPath} ]',
        'file': fileName,
        'data': []
      }

    else:
      lines = None
      with open(logPath,'r') as f:
        lines = f.readlines()

      response = {
        "status" : True,
        'message': f'Log file [ {logPath} ]',
        'file': fileName,
        'data': lines
      }

    return response
