import shlex
import signal
import time
import os
import sys
import subprocess
from threading import Timer
import json

class run_configClass:
    
    def __init__(self):
        self.cmd = 'sudo sysctl kernel.shmmni=32000'
        self.outputData = ''
        
    def do(self):
        child = subprocess.Popen(
                shlex.split(self.cmd),
                shell=False, 
                stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE
            )
        
        out, err = child.communicate()

        self.outputData = out.decode() + err.decode()


    def __str__(self):
        return self.outputData