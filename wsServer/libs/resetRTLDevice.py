import shlex
import subprocess
import os

class usbresetClass:
    
    def __init__(self):
        self.cmd = 'sudo lsusb'
        self.outputData = ''
        
    def do(self):
        child = subprocess.Popen(
                shlex.split(self.cmd),
                shell=False, 
                stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE
            )
        
        out, err = child.communicate()
        outputData = out.decode() + err.decode()

        bus = None
        port = None
        
        for line in outputData.splitlines():
            
            if (line.find('Realtek Semiconductor') != -1):
                parser = line.split(":")
                parser = parser[0].split(" ")
                bus = parser[1]
                port = parser[3]

        if ( bus != None ):
            self.cmd = 'sudo {0}/{1}/usbreset /dev/bus/usb/{2}/{3}'.format(os.getcwd(),'libs',bus,port)

            child = subprocess.Popen(
                shlex.split(self.cmd),
                shell=False, 
                stdin=subprocess.PIPE, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE
            )
        
            out, err = child.communicate()
            outputData = out.decode() + err.decode()
            
            self.outputData = outputData

        else:
            raise Exception('RTL device not found...')