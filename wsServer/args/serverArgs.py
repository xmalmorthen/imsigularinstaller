from optparse import OptionParser
import globals

parser = OptionParser(usage="%prog: [options]")
parser.add_option("-f", "--force", action="store_true",
                  help="Force reinit service")

(options, args) = parser.parse_args()
globals.ARGS = args
