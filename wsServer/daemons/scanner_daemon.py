#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import time
import sys

if not os.geteuid()==0:
    sys.exit('This script must be run as root!')

work_dir = '{0}/../'.format(os.path.dirname(os.path.realpath(__file__)))
os.chdir(work_dir)
sys.path.append(work_dir)

import globals
globals.init()

import logging
import time
from daemon import runner
class App():
  def __init__(self,logger):
    self.logger = logger
    self.stdin_path      = '/dev/null'
    self.stdout_path     = '/dev/tty'
    self.stderr_path     = '/dev/tty'
    self.pidfile_path    = '/var/run/{0}.pid'.format(globals.CNFG['daemons']['scanner'])
    self.pidfile_timeout = 1

  def setup_daemon_context(self, daemon_context):
    self.daemon_context = daemon_context

  def handle_exit(self, signum, frame):
    try:
      self.logger.info('scanner daemon stopping...')
      self.daemon_context.stop_event.set()
    except BaseException as exc:
      self.logger.exception(exc)

  def run(self):
    self.logger.info('scanner daemon running...')
    i = 1
    while True:
      logger.info("message %s" %i)
      i += 1
      time.sleep(1)

if __name__ == "__main__":

  try:
    logger = logging.getLogger(globals.CNFG['daemons']['scanner'])
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(message)s")
    handler = logging.FileHandler("/var/log/{0}.log".format(globals.CNFG['daemons']['scanner']))
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    app = App(logger = logger)

    serv = runner.DaemonRunner(app)
    serv.daemon_context.files_preserve=[handler.stream]
    serv.do_action()

  except Exception as err:
    #print ('__main__ ', err)
    pass
