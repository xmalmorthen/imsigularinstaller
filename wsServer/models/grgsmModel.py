import json

class scanner:
    def __init__(self):
        self._band = "PCS1900"
        self._samp_rate = 2e6
        self._ppm = 0
        self._gain = 24.0
        self._args = "RTL=0"
        self._speed = 4
        self._freq = 0
        self._id = 0
        self._btsInfo = ''

    def __str__(self):

      model = {
          'band': self.band,
          'samp_rate': self.samp_rate,
          'ppm': self.ppm,
          'gain': self.gain,
          'args': self.args,
          'speed': self.speed,
          'freq': self.freq,
          'id': self.id,
          'btsInfo': self._btsInfo
        }

      return json.dumps(model)

    @property
    def band(self):
        return self._band
    @band.setter
    def band(self, value):
        self._band = value

    @property
    def samp_rate(self):
        return self._samp_rate
    @samp_rate.setter
    def samp_rate(self, value):
        self._samp_rate = value

    @property
    def ppm(self):
        return self._ppm
    @ppm.setter
    def ppm(self, value):
        self._ppm = value

    @property
    def gain(self):
        return self._gain
    @gain.setter
    def gain(self, value):
        self._gain = value

    @property
    def args(self):
        return self._args
    @args.setter
    def args(self, value):
        self._args = value

    @property
    def speed(self):
        return self._speed
    @speed.setter
    def speed(self, value):
        self._speed = value

    @property
    def freq(self):
        return self._freq
    @freq.setter
    def freq(self, value):
        self._freq = value

    @property
    def id(self):
        return self._id
    @id.setter
    def id(self, value):
        self._id = value

    @property
    def btsInfo(self):
        return self._btsInfo
    @btsInfo.setter
    def btsInfo(self, value):
        self._btsInfo = value
