# -*- coding: utf-8 -*-

import libs.configLib as configLib
import libs.databaseLib as databaseLib
import globals

class imsi():
    def __init__(self):
        pass

    @property
    def isDbOnline(self):
        try:
            globals.DB.run_query('select sqlite_version()')
            return True
        except Exception as err:
            return False

    @property
    def isScannDaemonUp(self):
      try:
        import psutil
        return globals.CNFG['daemons']['scanner'] in (p.name() for p in psutil.process_iter())
      except Exception as err:
        return False
