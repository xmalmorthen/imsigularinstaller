# -*- coding: utf-8 -*-

import os

def init(configFileName = None):

    global APP
    global ARGS
    global CONFIGFILENAME
    global SOCKETIO
    global CNFG
    global DB
    global BTSDB
    global PIDFILE_PATH
    global SUPERLOGGER
    global FULLRESIDUALFILESPATH
    global LOGSPATH
    global SERVICELISTMESSAGES
    global GLOBALMSG
    global RESSETING
    global SOCKETCLIENTS

    GLOBALMSG = None
    RESSETING = False
    SOCKETCLIENTS = []

    CONFIGFILENAME = 'config.ini' if configFileName == None else configFileName

    import libs.configLib as configLib

    __cnfg = configLib.config()
    CNFG = __cnfg.config

    __dbCnfg = {
      "connector": __cnfg.get('db','connector'),
      "host" : __cnfg.get('db','host'),
      "port" : __cnfg.get('db','port'),
      "username" : __cnfg.get('db','usr'),
      "password" : __cnfg.get('db','pwd'),
      "dbname" : __cnfg.get('db','dbDefault')
    }

    __BTSdbCnfg = {
      "connector": __cnfg.get('db','connector'),
      "host" : __cnfg.get('db','host'),
      "port" : __cnfg.get('db','port'),
      "username" : __cnfg.get('db','usr'),
      "password" : __cnfg.get('db','pwd'),
      "dbname" : __cnfg.get('db','btsdb')
    }

    import libs.databaseLib as databaseLib
    DB = databaseLib.database(__dbCnfg)
    BTSDB = databaseLib.database(__BTSdbCnfg)

    SOCKETIO = None

    SERVICELISTMESSAGES = {
        'rtl_testGainOnly':'getting GSM gain levels',
        'rtl_test':'getting GSM ppm factor',
        'grgsm_scanner':"scanning BTS's",
        'grgsm_scannerKill': "Killing BTS's scanning",
        'grgsm_livemon': "scanning BTS",
        'grgsm_livemonKill': "Killing BTS scanning",
        'reset': "System resetting"
    }

    LOGSPATH = '{0}/{1}'.format( os.getcwd(), __cnfg.get('general','logsPath') )
    if not os.path.exists(LOGSPATH):
        os.mkdir(LOGSPATH)
