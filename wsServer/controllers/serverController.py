# -*- coding: utf-8 -*-

import sys, os
from gevent import monkey
monkey.patch_all()
import html
import json
from flask import Flask, render_template, request
from flask_socketio import SocketIO, join_room, leave_room
from threading import Thread
from actions import loopBackAction
from libs import utils
import globals
from libs.loggerLib import Logger
import inspect
from libs.auth import AuthClass as authAdm

app = Flask(__name__)
app.config['SECRET_KEY'] = 'imsigular'
socketio = SocketIO(app,cors_allowed_origins='*',cookie=None)

def start():
  try:

    os.chdir(globals.APP.executionPath)

    import libs.configLib as configLib

    HOST = configLib.config().get('socket','host')
    PORT = int(configLib.config().get('socket','port'))
    
    str=''
    for el in os.getcwd().split('/')[:-1]:
      str += f'{el}/' 
    
    CERTFILE = f"{str}" + configLib.config().get('socket','certFile')
    KEYFILE = f"{str}" + configLib.config().get('socket','keyFile')
    
    globals.SOCKETIO = socketio
    globals.RESSETING = False

    '''
    INIT LOOP BACK STATUS SERVICE
    '''
    import actions.loopBackAction as loopBackAction
    loopBackStatus = loopBackAction.status()
    loopBackStatus.setDaemon(True)
    loopBackStatus.start()

    loopBackRTLStatus = loopBackAction.RTLStatusLoop()
    loopBackRTLStatus.setDaemon(True)
    loopBackRTLStatus.start()

    RTLCalibrate = loopBackAction.RTLCalibrate()
    RTLCalibrate.setDaemon(True)
    if 'start' in globals.ARGS or 'restart' in globals.ARGS:
      RTLCalibrate.start()

    pid = ''
    if os.path.exists(globals.PIDFILE_PATH):
      f = open(globals.PIDFILE_PATH, "r")
      pid = f.read()
      f.close()
      qry = "UPDATE `main`.`marun` SET `pid`= {0} WHERE `status` = 1".format(pid)
      globals.DB.run_query(qry)
    else:
      msg = 'No se pudo leer el archivo de pid {0}'.format(globals.PIDFILE_PATH)
      Logger.set('error',f'{inspect.stack()[0][3]} => {msg}' )
      #print('No se pudo leer el archivo de pid {0}'.format(globals.PIDFILE_PATH))
      exit()

    Logger.set('info',f'{inspect.stack()[0][3]} => Socket server running in PID {int(pid)}' )

    socketio.run(
      app,
      host= HOST,
      certfile= CERTFILE,
      keyfile = KEYFILE,
      port= PORT
    )

  except Exception as ex:
    raise ex

@socketio.on('connect', namespace='/imsi')
def ws_conn():
  Logger.set('debug',f'{inspect.stack()[0][3]} | init')

  try:

    Logger.set('debug',f'{inspect.stack()[0][3]} => client connected' )

    from actions import imsiStatusAction
    response = imsiStatusAction.get()

    socketio.emit('status', response,namespace="/imsi",room=request.sid)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.status [ {response} ]')

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} => | end')

@socketio.on('disconnect', namespace='/imsi')
def ws_disconn():
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )
  try:
    Logger.set('info',f'{inspect.stack()[0][3]} => client disconnected' )
  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' GET SERVICES STATUS '''
@socketio.on('status', namespace='/imsi')
def ws_status(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    from actions import imsiStatusAction
    response = imsiStatusAction.get()

    socketio.emit('status', response,namespace="/imsi",room=request.sid)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.status [ {response} ]')

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' GET SERVICES STATUS '''
@socketio.on('rtlGain', namespace='/imsi')
def ws_rtlGain(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    loopBackAction.RTLStatus.sendStatus(True,'rtl_testGainOnly')

    from actions.imsiActions import rtlGainsClass
    rtlGains = rtlGainsClass()
    rtlGains.start()
    rtlGains.join()

    socketio.emit('rtlGain', rtlGains.response,namespace="/imsi",room=request.sid)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.rtlGain [ {rtlGains.response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' grgsm_scanner '''
@socketio.on('grgsmscanner', namespace='/imsi')
def grgsmscanner(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    response = None

    gain = 0
    if not "gain" in data:
      response = { 'status': False, 'message': 'GAIN value not specified' }
    else:
      try:
        gain = float(data['gain'])
      except ValueError:
        response = { 'status': False, 'message': 'GAIN value must be number' }

    if response != None:
        socketio.emit('grgsmscanner', response ,namespace="/imsi",room=request.sid)

    testMode = data['testMode'] if "testMode" in data else False

    loopBackAction.RTLStatus.sendStatus(True,'grgsm_scanner')

    from actions import imsiActions
    import models.grgsmModel as grgsmModel

    grgsmScannerModel = grgsmModel.scanner()
    grgsmScannerModel.gain = gain

    if 'speed' in data:
      try:
        grgsmScannerModel.speed = int(data['speed'])
      except ValueError:
        pass

    grgsm_scanner = imsiActions.grgsm_scanner(grgsmScannerModel,testMode)
    grgsm_scanner.start()
    grgsm_scanner.join()

    response = {
      "status" : True,
      "action" : 'grgsmscanner',
      "message" : "action executed"
    }
    response['data'] = grgsm_scanner.response

    socketio.emit('grgsmscanner', response ,namespace="/imsi", broadcast=True)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.grgsmscanner [ {response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' grgsm_scanner kill'''
@socketio.on('grgsmscannerKill', namespace='/imsi')
def grgsmscannerKill(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    loopBackAction.RTLStatus.sendStatus(True,'grgsm_scannerKill')

    from libs.grgsm_scanner import grgsm_scannerClass
    grgsmScanner = grgsm_scannerClass(gain = None,ppm = None,speed= None)
    grgsmScanner.kill()

    response = {
      "status" : True,
      "action" : 'grgsmscannerKill',
      "message" : "action executed"
    }

    socketio.emit('grgsmscannerKill', response ,namespace="/imsi", broadcast=True)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.grgsmscannerKill [ {response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' grgsm_livemon_nogui '''
@socketio.on('grgsmlivemon', namespace='/imsi')
def grgsmlivemon(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    response = { 'status': True, 'message': [] }

    testMode = data['testMode'] if "testMode" in data else False

    idBts = 0
    if not "id" in data:
      response['status'] = False
      response['message'].append('ID value of bts not specified')
    else:
      try:
        idBts = int(data['id'])
      except ValueError:
        response['status'] = False
        response['message'].append('ID value of bts must be number')

    freq = 0
    if not "freq" in data:
      response['status'] = False
      response['message'].append('FREQ value not specified')
    else:
      freq = data['freq']

    gain = 0
    if not "gain" in data:
      response['status'] = False
      response['message'].append('GAIN value not specified')
    else:
      try:
        gain = float(data['gain'])
      except ValueError:
        response['status'] = False
        response['message'].append('GAIN value must be number')

    ppm = 0
    if not "ppm" in data:
      response['status'] = False
      response['message'].append('PPM value not specified')
    else:
      try:
        ppm = int(data['ppm'])
      except ValueError:
        response['status'] = False
        response['message'].append('PPM value must be number')

    btsInfo = data['btsInfo'] if "btsInfo" in data else ''

    if response != None:
      socketio.emit('grgsmlivemon', response ,namespace="/imsi",room=request.sid)

    loopBackAction.RTLStatus.sendStatus(True,'grgsm_livemon',{ 'idBts': idBts, 'btsInfo': btsInfo })

    from actions import imsiActions
    import models.grgsmModel as grgsmModel

    grgsmLivemonModel = grgsmModel.scanner()
    grgsmLivemonModel.freq = freq
    grgsmLivemonModel.gain = gain
    grgsmLivemonModel.ppm = ppm
    grgsmLivemonModel.id = idBts
    grgsmLivemonModel.btsInfo = btsInfo

    grgsm_livemon = imsiActions.grgsm_livemon(grgsmLivemonModel,testMode)
    grgsm_livemon.start()
    grgsm_livemon.join()

    response = {
      "status" : True,
      "action" : 'grgsmlivemon',
      "message" : "action executing"
    }
    response['data'] = grgsm_livemon.response

    socketio.emit('grgsmlivemon', response ,namespace="/imsi", broadcast=True)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.grgsmlivemon [ {response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' grgsm_livemon_nogui kill'''
@socketio.on('grgsmlivemonKill', namespace='/imsi')
def grgsmlivemonKill(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    loopBackAction.RTLStatus.sendStatus(True,'grgsm_livemonKill')

    from libs.grgsm_livemon import grgsm_livemon_nogui
    grgsmlivemon = grgsm_livemon_nogui(freq= None, gain = None,ppm = None, testMode= False)
    grgsmlivemon.kill()

    response = {
      "status" : True,
      "action" : 'grgsmlivemonKill',
      "message" : "action executed"
    }

    socketio.emit('grgsmlivemonKill', response ,namespace="/imsi", broadcast=True)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.grgsmlivemonKill [ {response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

''' reset '''
@socketio.on('reset', namespace='/imsi')
def reset(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    loopBackAction.RTLStatus.sendStatus(True,'reset')

    from libs.reset import resetClass
    reset = resetClass()
    reset.do()

    response = {
      "status" : True,
      "action" : 'reset',
      "message" : "action executed"
    }

    socketio.emit('reset', response ,namespace="/imsi", broadcast=True)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.reset [ {response} ]' )

    loopBackAction.RTLStatus.sendStatus(False)

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )


@socketio.on('logFile', namespace='/imsi')
def logFile(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    socketio.emit('logFile', Logger.get() ,namespace="/imsi",room=request.sid)

    Logger.set('info',f'{inspect.stack()[0][3]} => emit.logFile sended log file' )

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )


@socketio.on('admCode', namespace='/imsi')
def admCode(data):
  try:

    if not 'code' in data:
      raise Exception('Admin code not specified')

    if data['code'] in authAdm.admCodes():
      socketio.emit('admCode', { 'status': True, 'message': 'Admin code correct' } ,namespace="/imsi",room=request.sid)
    else:
      socketio.emit('admCode', { 'status': False, 'message': 'Admin code incorrect' } ,namespace="/imsi",room=request.sid)

  except Exception as ex:
    socketio.emit('admCode', { 'status': False, 'message': str(ex) } ,namespace="/imsi",room=request.sid)


@socketio.on('auth', namespace='/imsi')
def auth(data):
  try:

    if not 'code' in data:
      raise Exception('Admin code not specified')

    if data['code'] in authAdm.admCodes():

      jwToken = None

      if not authAdm.checkIfAuth(sid=request.sid)['authenticated']:
        jwToken = authAdm.addClient(sid=request.sid)

      socketio.emit('auth', { 'status': True, 'message': 'Admin code correct', 'sid': request.sid, 'jwToken': jwToken } ,namespace="/imsi",room=request.sid)

    else:
      socketio.emit('auth', { 'status': False, 'message': 'Admin code incorrect' } ,namespace="/imsi",room=request.sid)

  except Exception as ex:
    socketio.emit('auth', { 'status': False, 'message': str(ex) } ,namespace="/imsi",room=request.sid)

@socketio.on('authCheck', namespace='/imsi')
def authCheck(data):
  try:

    if not 'jwToken' in data:
      raise Exception('jwToken not specified')

    jwtData = authAdm.validateToken(data['jwToken'])

    authAdm.checkIfAuth(jwtData['sid'])

    socketio.emit('authCheck', { 'status': True, 'message': 'With session' } ,namespace="/imsi",room=request.sid)

  except Exception as ex:
    socketio.emit('authCheck', { 'status': False, 'message': str(ex) } ,namespace="/imsi",room=request.sid)


@socketio.on('shutDownSystem', namespace='/imsi')
def shutDownSystem(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    Logger.set('info',f'{inspect.stack()[0][3]} => Shutting down system' )

    os.system("shutdown -h now")

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )

@socketio.on('restartSystem', namespace='/imsi')
def restartSystem(data):
  Logger.set('debug',f'{inspect.stack()[0][3]} | init' )

  try:

    Logger.set('info',f'{inspect.stack()[0][3]} => Restarting system' )

    os.system("shutdown -r now")

  except Exception as ex:
    msg = f'{inspect.stack()[0][3]} => [ {ex} ]'
    print(msg)
    Logger.set('error',msg)

  Logger.set('debug',f'{inspect.stack()[0][3]} | end' )
