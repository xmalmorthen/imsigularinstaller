# -*- coding: utf-8 -*-

import threading
import subprocess
import psutil
import shlex
import os
import time
import globals
import libs.configLib as configLib

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

from models import imsiModel
from libs.lsusbRTL import lsusbRTLClass

def get():

    fullResidualFilesPath = '{0}/{1}'.format(os.getcwd(),globals.CNFG.get('general','residualFilesPath'))

    if not os.path.exists(fullResidualFilesPath):
        os.mkdir(fullResidualFilesPath)

    globals.FULLRESIDUALFILESPATH = fullResidualFilesPath

    #scannDaemon= scannDaemonClass()
    net= netClass()
    db= dbClass()
    radios= radiosClass()

    #scannDaemon.start()
    net.start()
    db.start()
    radios.start()

    #scannDaemon.join()
    net.join()
    db.join()
    radios.join()


    generalStatus = db.response["status"] \
                    and radios.response["status"]
                    #and net.response["status"] \
                    #and scannDaemon.response["status"] \

    response = {
        "status" : generalStatus,
        "action" : 'status',
        "message" : "action executed" if generalStatus else "One or more services are inactive",
        "data" : {
            "wsVersion": configLib.config().get('general','version'),
            "db" : db.response,
            "radios": radios.response,
            "net": net.response,
            #"daemons": {
            #  "scann": scannDaemon.response
            #},
        }
    }

    return response

''' NET '''
class netClass(threading.Thread):
    def __init__(self):
        self.response = None
        threading.Thread.__init__(self)

    def run(self):
        try:
            urlopen("http://www.google.com/",timeout=1)
            self.response = { "status": True, "message": 'With internet connection'}
        except Exception as err:
            self.response = { "status": False, "message": 'Without internet connection'}

''' DATA BASE '''
class dbClass(threading.Thread):
    def __init__(self):
        self.response = None
        threading.Thread.__init__(self)

    def run(self):
        status = imsiModel.imsi().isDbOnline
        self.response = { "status": status, "message": 'With data base connection' if status else 'Without data base connection'}

''' SCANN DAEMON '''
class scannDaemonClass(threading.Thread):
    def __init__(self):
        self.response = None
        threading.Thread.__init__(self)

    def run(self):
        status = imsiModel.imsi().isScannDaemonUp
        self.response = { "status": status, "message": 'Daemon running' if status else 'Daemon down'}

''' RADIOS '''
class radiosClass(threading.Thread):
    def __init__(self):
        self.response = None
        threading.Thread.__init__(self)

    def run(self):
        try:

            lsusbRTL = lsusbRTLClass()
            response = lsusbRTL.do()
            self.response = { "status": response, "message": "RTL device " + ( 'found' if response else 'not found')  }

        except Exception as err:
            self.response = {
                             "status": False,
                             "message": "A problem occurred while trying to run the command",
                             "trace": str(err)
                             }
