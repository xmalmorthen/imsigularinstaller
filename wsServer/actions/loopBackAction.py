# -*- coding: utf-8 -*-

import threading
import time
import globals
import json
from libs.rtl_test import rtl_testClass
import subprocess
import shlex
from libs import utils
from libs.loggerLib import Logger
import inspect

class status(threading.Thread):
    def __init__(self):
      self.response = None
      threading.Thread.__init__(self)

    def run(self):
      from actions import imsiStatusAction
      while True:
        response = imsiStatusAction.get()
        #print ( '[ loopBack ] action => status | response => [ {0} ]'.format(response) )
        globals.SOCKETIO.emit('status', response,namespace="/imsi", broadcast=True)

        #Logger.set('info', f'status.{inspect.stack()[0][3]} => emit.status [ {response} ]' )

        time.sleep(3)

class RTLStatusLoop(threading.Thread):
    def __init__(self):
      self.response = None
      threading.Thread.__init__(self)

    def run(self):
      while True:
        try:

            qry = "SELECT service,jsonData FROM `main`.`servicestatus` WHERE status = '1' GROUP by service"
            qryResponse = globals.DB.run_query(qry)

            servicesModel = []
            for item in qryResponse:
                modelResponse =   {
                    'service': item[0],
                    'message': globals.SERVICELISTMESSAGES.get(item[0],'service action not registered [ {0}]'.format(item[0]) )
                  }

                try:
                  modelResponse['data'] = json.loads(item[1])
                except Exception as errParse:
                  modelResponse['data'] = ''

                servicesModel.append(modelResponse)

            response = {
              'inUse' : True if len(qryResponse) > 0 else False,
              'service' : servicesModel
            }

            globals.SOCKETIO.emit('rtlDeviceStatus', response,namespace="/imsi", broadcast=True)

            #Logger.set('info', f'RTLStatusLoop.{inspect.stack()[0][3]} => emit.rtlDeviceStatus [ {response} ]' )

            time.sleep(1)

        except Exception as ex:
          msg = f'RTLStatusLoop.{inspect.stack()[0][3]} => [ {ex} ]'
          #print(msg)
          Logger.set('error',msg)

class RTLStatus():
    @staticmethod
    def sendStatus(status, service = '', aditionalData = {}):

      if len(service):

        servicesModel = [
          {
            'service': service,
            'message': globals.SERVICELISTMESSAGES.get(service,'service action not registered [ {0} ]'.format(service) ),
            'data': aditionalData
          }
        ]

        response = {
          'inUse' : status,
          'service' : servicesModel
        }

        globals.SOCKETIO.emit('rtlDeviceStatus', response ,namespace="/imsi", broadcast=True)

        Logger.set('info', f'{inspect.stack()[0][3]} => emit.rtlDeviceStatus [ {response} ]' )


class RTLCalibrate(threading.Thread):
    def __init__(self):
      threading.Thread.__init__(self)

    def run(self):

      rtlTest = rtl_testClass(backMode=True)

      while True:
        Logger.set('debug', f'RTLCalibrate.{inspect.stack()[0][3]} => | init')
        try:

          qry = "SELECT id FROM `main`.`servicestatus` WHERE status = '1' GROUP by service"
          qryResponse = globals.DB.run_query(qry)

          #print(len(qryResponse),globals.RESSETING)

          if (len(qryResponse) == 0) and globals.RESSETING == False:
            Logger.set('debug', f'{inspect.stack()[0][3]} => RTLCalibrate in progress')

            try:
              rtlTest.do()

            except Exception as rtlTestEx:
              msg = f'RTLCalibrate.{inspect.stack()[0][3]} => [ {rtlTestEx} ]'
              #print(msg)
              Logger.set('error',msg)

              time.sleep(5)

            if rtlTest.outputModel:
              globals.SOCKETIO.emit('RTLCalibrate', rtlTest.outputModel,namespace="/imsi", broadcast=True)
              Logger.set('info', f'RTLCalibrate.{inspect.stack()[0][3]} => emit.RTLCalibrate [ {rtlTest.outputModel} ]')

          else:
            Logger.set('debug', f'RTLCalibrate.{inspect.stack()[0][3]} => RTLCalibrate suspended  [ {len(qryResponse)} | {globals.RESSETING} ]')
            time.sleep(15)
            if (len(qryResponse) == 0) and globals.RESSETING == True:
              globals.RESSETING = False

        except Exception as ex:
          msg = f'RTLCalibrate.{inspect.stack()[0][3]} => [ {ex} ]'
          #print(msg)
          Logger.set('error',msg)

        Logger.set('debug', f'RTLCalibrate.{inspect.stack()[0][3]} | end')

    @staticmethod
    def kill():
        Logger.set('debug', f'RTLCalibrate.{inspect.stack()[0][3]} | init')

        servicesList = ['rtl_test','usbfs_memory_mb']

        Logger.set('debug', f'{inspect.stack()[0][3]} => killing {servicesList}')

        utils.kill_pid(utils.get_pid(servicesList))

        Logger.set('debug', f'{inspect.stack()[0][3]} => killed {servicesList}')

        Logger.set('debug', f'RTLCalibrate.{inspect.stack()[0][3]} | end')
