# -*- coding: utf-8 -*-

import threading
import subprocess
import uuid
import json
import time
import os
import libs.configLib as configLib
import libs.databaseLib as databaseLib
import models.grgsmModel as grgsmModel
from libs import utils
import globals as globals

class rtlGainsClass(threading.Thread):
    def __init__(self):
        self.response = None
        threading.Thread.__init__(self)

    def run(self):
        try:
            from libs.rtl_test import rtl_testGainOnlyClass
            rtlTest = rtl_testGainOnlyClass()
            rtlTest.do()

            self.response = rtlTest.outputModel

        except Exception as err:
            self.response = {
                "status": False,
                "message": "A problem occurred while trying to run the command",
                "trace": str(err)
                }

''' grgsm_scanner '''
class grgsm_scanner(threading.Thread):
    def __init__(self, grgsmScannerModel = None, testMode = False):
        self.testMode = testMode
        self.response = None

        if (grgsmScannerModel != None):
            if (isinstance(grgsmScannerModel,grgsmModel.scanner)):
                self.grgsmScannerModel = grgsmScannerModel
            else:
                raise Exception("Data type for grgsmScannerModel must be grgsmModel.scanner type")
        else: # SET DEFAULT PARAMETERS
            self.grgsmScannerModel = grgsmModel.scanner()

        threading.Thread.__init__(self)

    def run(self):

        from libs.run_config import run_configClass
        from libs.rtl_test import rtl_testClass
        from libs.grgsm_scanner import grgsm_scannerClass

        try:

          if not self.testMode:
            runConfig = run_configClass()
            runConfig.do()

            rtlTest = rtl_testClass()
            rtlTest.do()

            if self.grgsmScannerModel.ppm == 0:
              self.grgsmScannerModel.ppm = rtlTest.outputModel['PPM']

          grgsmScanner = grgsm_scannerClass(
            gain = self.grgsmScannerModel.gain,
            ppm = self.grgsmScannerModel.ppm,
            speed= self.grgsmScannerModel.speed,
            testMode= self.testMode
          )
          grgsmScanner.do()

          if (self.testMode):
            time.sleep(1)

          response = {
              "status": True,
              "message": "scanned completed",
              "config": {
                'gain' : self.grgsmScannerModel.gain,
                'ppm' : self.grgsmScannerModel.ppm,
                'speed' : self.grgsmScannerModel.speed,
              },
              "data": grgsmScanner.outputModel
          }

          self.response = response

        except Exception as err:
            self.response = { "status": False, "message": str(err)}

''' grgsm_livemon '''
class grgsm_livemon(threading.Thread):
    def __init__(self, grgsmLivemonModel = None, testMode = False):
        self.testMode = testMode
        self.response = None

        self.residualFileName = 'tsharkResponse'

        self.grgsmLivemonModel = None
        if (grgsmLivemonModel != None):
            if (isinstance(grgsmLivemonModel,grgsmModel.scanner)):
                self.grgsmLivemonModel = grgsmLivemonModel
            else:
                raise Exception("Data type for grgsmLivemonModel must be grgsmModel.scanner type")
        else: # SET DEFAULT PARAMETERS
            self.grgsmLivemonModel = grgsmModel.scanner()

        threading.Thread.__init__(self)

    def run(self):

        id = None

        try:

          service = 'grgsm_livemon'

          qry = "SELECT * FROM `main`.`vw_scanproc` WHERE serviceName = '{0}' and serviceStatus = '1' ORDER by fIns DESC LIMIT 1".format(service)
          responseQuery = globals.DB.run_query(qry)

          response = None

          if len(responseQuery) != 0:

              response = {
                  "status": True,
                  "message": "scanning in proc",
                  "config": {
                    'freq' : responseQuery[0][4],
                    'gain' : responseQuery[0][5],
                    'ppm' : responseQuery[0][6]
                  },
                  "markers" : {
                    'serviceId' : responseQuery[0][0]
                  }
              }

          else:

              residualFilePath = '{0}/{1}'.format(globals.FULLRESIDUALFILESPATH,self.residualFileName)

              from libs.grgsm_livemon import grgsm_livemon_nogui, tsharkParser

              __grgsm_livemon_nogui = grgsm_livemon_nogui(
                freq = self.grgsmLivemonModel.freq,
                gain = self.grgsmLivemonModel.gain,
                ppm = self.grgsmLivemonModel.ppm,
                testMode= self.testMode
              )
              __grgsm_livemon_nogui.setDaemon(True)
              __grgsm_livemon_nogui.start()

              jsonData = self.grgsmLivemonModel

              qry = "INSERT INTO `main`.`servicestatus`(`service`,`jsonData`) VALUES ('{0}','{1}')".format(service,jsonData)
              globals.DB.run_query(qry)
              id = globals.DB.lastrowid

              __tsharkParser = tsharkParser(
                  residualFileName= residualFilePath,
                  servideId= id,
                  freq = self.grgsmLivemonModel.freq,
                  gain = self.grgsmLivemonModel.gain,
                  ppm = self.grgsmLivemonModel.ppm,
                  btsInfo = self.grgsmLivemonModel.btsInfo,
                  testMode= self.testMode
                )



              time.sleep(5)

              __tsharkParser.setDaemon(True)
              __tsharkParser.start()

              response = {
                  "status": True,
                  "message": "scanning in proc",
                  "config": {
                    'freq' : self.grgsmLivemonModel.freq,
                    'gain' : self.grgsmLivemonModel.gain,
                    'ppm' : self.grgsmLivemonModel.ppm,
                  },
                  "markers" : {
                    'serviceId' : id
                  }
              }

          self.response = response

        except Exception as err:
            #print ('grgsm_livemon ',err)
            if id != None:
              jsonData = json.dumps(err)
              timestamp = utils.getTimeStamp()

              qry = "UPDATE `main`.`servicestatus` SET 'status'='2', `jsonData`= '{0}', `fAct` = '{1}' WHERE `id` = {2}".format(jsonData,timestamp,id)
              globals.DB.run_query(qry)

            self.response = { "status": False, "message": str(err)}
