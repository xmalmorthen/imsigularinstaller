(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+7GD":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shared/errs/err404/err404.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>err404 works!</p>\n");

/***/ }),

/***/ "+jLx":
/*!**************************************!*\
  !*** ./src/app/pages/page.module.ts ***!
  \**************************************/
/*! exports provided: PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageModule", function() { return PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @agm/core */ "LSHg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "SVse");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "cUpR");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main/main.component */ "/s1f");
/* harmony import */ var _shared_errs_err404_err404_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shared/errs/err404/err404.component */ "AXJl");
/* harmony import */ var _sweetalert2_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @sweetalert2/ngx-sweetalert2 */ "hvj1");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _pipes_check_proc_is_running_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../pipes/check-proc-is-running.pipe */ "yopv");
/* harmony import */ var _pipes_parse_rtl_pipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../pipes/parse-rtl.pipe */ "WjsP");
/* harmony import */ var _pipes_filter_mnc_pipe__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../pipes/filter-mnc.pipe */ "7iTK");
/* harmony import */ var angular_split__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! angular-split */ "cdP3");













$.LoadingOverlaySetup({
    background: "rgba(52, 58, 64, 0.5)",
    image: "",
    fontawesome: "fa fa-cog fa-spin",
    fontawesomeAutoResize: true,
    fontawesomeOrder: 1,
    text: '',
    textAutoResize: true,
    textResizeFactor: 1,
    direction: 'column',
    fade: [100, 100],
    zIndex: 9999999
});
let PageModule = class PageModule {
    constructor() {
        webNotification.requestPermission(function onRequest(granted) {
            /*
            if (granted) {
                console.log('Permission Granted.');
            } else {
                console.log('Permission Not Granted.');
            }
            */
        });
    }
};
PageModule.ctorParameters = () => [];
PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _main_main_component__WEBPACK_IMPORTED_MODULE_5__["MainComponent"],
            _shared_errs_err404_err404_component__WEBPACK_IMPORTED_MODULE_6__["Err404Component"],
            _pipes_check_proc_is_running_pipe__WEBPACK_IMPORTED_MODULE_9__["CheckProcIsRunningPipe"],
            _pipes_parse_rtl_pipe__WEBPACK_IMPORTED_MODULE_10__["ParseRTLPipe"],
            _pipes_filter_mnc_pipe__WEBPACK_IMPORTED_MODULE_11__["FilterMNCPipe"],
            _pipes_filter_mnc_pipe__WEBPACK_IMPORTED_MODULE_11__["filterBtsScanningPipe"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _agm_core__WEBPACK_IMPORTED_MODULE_1__["AgmCoreModule"].forRoot({ apiKey: src_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].googleMap.apiKey }),
            _sweetalert2_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_7__["SweetAlert2Module"],
            angular_split__WEBPACK_IMPORTED_MODULE_12__["AngularSplitModule"]
        ]
    })
], PageModule);



/***/ }),

/***/ "/s1f":
/*!**********************************************!*\
  !*** ./src/app/pages/main/main.component.ts ***!
  \**********************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_main_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./main.component.html */ "bBbC");
/* harmony import */ var _main_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main.component.scss */ "QM7E");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var ng_connection_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng-connection-service */ "36es");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-cookie-service */ "b6Qw");
/* harmony import */ var src_app_services_websocket_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/websocket.service */ "Gyf/");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "PSD3");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);










let MainComponent = class MainComponent {
    constructor(activatedRoute, cookieService, elementRef, wsService, connectionService) {
        this.activatedRoute = activatedRoute;
        this.cookieService = cookieService;
        this.elementRef = elementRef;
        this.wsService = wsService;
        this.connectionService = connectionService;
        this.projectVersions = null;
        this.inAction = false;
        this.rtlInUse = false;
        this.testModeParam = false;
        this.withInternet = navigator.onLine;
        this.collapsedReset = false;
        this.sessionSwalInterval = null;
        this.mapModel = {
            currentCords: { lat: 0, lng: 0 },
            mapCenterCors: { lat: 0, lng: 0 },
            dragCors: { drageed: false, lat: 0, lng: 0 },
            zoom: 15
        };
        this.RTLCalibrateModel = null;
        this.connectionService.monitor().subscribe((currentState) => {
            this.withInternet = currentState;
        });
        this.statusModel = { proc: null };
        this.systemResetModel = { proc: false };
        this.rtlModel = { proc: false, gainSelected: -1, speed: 4 };
        this.grgsmscannerModel = { proc: false, testMode: this.testModeParam };
        this.grgsmslivemoonModel = { proc: false, testMode: this.testModeParam };
        this.tsharkModel = {};
        this.wsService.socketConnected.subscribe((status) => {
            this.__init();
        });
        this.__init();
    }
    beforeUnloadHandler(event) {
        this.cookieService.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].cookieImsiInstanceName);
    }
    __init() {
        //console.log('models init');
        this.statusModel = { proc: null };
        this.systemResetModel = { proc: false };
        this.rtlModel = { proc: false, gainSelected: -1, speed: 4 };
        this.grgsmscannerModel = { proc: false, testMode: this.testModeParam };
        this.grgsmslivemoonModel = { proc: false, testMode: this.testModeParam };
        this.tsharkModel = {};
        //TODO: Comment this line [ is for simulate test mode response in scan bts ]
        //TODO: Only Test
        //this.grgsmscannerModel.testMode = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.close();
        this.sessionSwal();
    }
    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.testModeParam = params['test'] ? (params['test'].toLowerCase() == 'true' ? true : false) : false;
        });
        this.wsService.listen('status')
            .subscribe((data) => this.statusListener(data));
        this.wsService.listen('reset')
            .subscribe((data) => this.systemResetListener(data));
        this.wsService.listen('rtlDeviceStatus')
            .subscribe((data) => this.rtlDeviceListener(data));
        this.wsService.listen('rtlGain')
            .subscribe((data) => this.rtlGainsListener(data));
        this.wsService.listen('grgsmscanner')
            .subscribe((data) => this.grgsmScannerListener(data));
        this.wsService.listen('grgsmscannerKill')
            .subscribe((data) => this.grgsmScannerKillListener(data));
        this.wsService.listen('grgsmlivemoon')
            .subscribe((data) => this.grgsmLivemoonListener(data));
        this.wsService.listen('grgsmlivemonKill')
            .subscribe((data) => this.grgsmLivemoonKillListener(data));
        this.wsService.listen('tshark')
            .subscribe((data) => this.tsharkListener(data));
        this.wsService.listen('RTLCalibrate')
            .subscribe((data) => this.RTLCalibrateListener(data));
        this.wsService.listen('logFile')
            .subscribe((data) => this.logFileListener(data));
        this.wsService.socketConnected.subscribe((status) => { });
        if (navigator.geolocation)
            navigator.geolocation.getCurrentPosition((position) => {
                if (position) {
                    this.mapModel.currentCords = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    this.mapModel.mapCenterCors = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    this.mapModel.dragCors = {
                        drageed: false,
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                }
            });
        this.wsService.projectVersions.subscribe((data) => {
            this.projectVersions = { imsi: data.imsi, ws: data.ws };
        });
    }
    ngOnDestroy() {
        clearInterval(this.sessionSwalInterval);
        this.sessionSwalInterval = null;
    }
    statusAction(evt) {
        this.statusModel.proc = true;
        this.wsService.emmit('status', {});
    }
    signOut(evt) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: 'Confirm',
            html: "sign off",
            icon: 'question',
            showCancelButton: true,
            backdrop: `
        rgba(0,0,0,0.97)
        left top
        no-repeat
      `,
        }).then((result) => {
            if (!result.isDismissed) {
                this.wsService.delSessCookie();
            }
        });
    }
    statusListener(data) {
        //console.log(data);
        this.statusModel.model = data;
        this.statusModel.proc = false;
        if (!this.statusModel.model.data.radios.status) {
            if (!this.collapsedReset) {
                this.collapsedReset = true;
                this.wsService.emmit('reset', {});
            }
            $('#mainSection').LoadingOverlay("show", { fontawesome: 'fa fa-exclamation-triangle', fontawesomeColor: '#FF0000', fontawesomeResizeFactor: 5, text: 'GSM DEVICE DISCONNECTED', textColor: '#FF0000' });
        }
        else {
            this.collapsedReset = false;
            $('#mainSection').LoadingOverlay("hide", true);
        }
    }
    systemResetAction(evt) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: 'Confirm',
            html: "system reset",
            icon: 'question',
            showCancelButton: true,
            backdrop: `
        rgba(0,0,0,0.97)
        left top
        no-repeat
      `,
        }).then((result) => {
            if (!result.isDismissed) {
                this.inAction = true;
                this.systemResetModel.proc = true;
                this.wsService.emmit('reset', {});
                setTimeout(() => {
                    window.location.reload();
                }, 500);
            }
        });
    }
    eventIMSIComputer(evt, type) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            allowOutsideClick: false,
            title: 'Confirm',
            html: `${type === 0 ? 'restart' : 'shutdown'} system`,
            icon: 'question',
            showCancelButton: true,
            backdrop: `
        rgba(0,0,0,0.97)
        left top
        no-repeat
      `,
        }).then((result) => {
            if (!result.isDismissed) {
                this.inAction = true;
                this.systemResetModel.proc = true;
                this.wsService.emmit(type == 0 ? 'restartSystem' : 'shutDownSystem', {});
            }
        });
    }
    getLogFile(evt) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                allowOutsideClick: false,
                title: 'Admin action',
                icon: 'info',
                showCancelButton: true,
                backdrop: `
        rgba(0,0,0,0.97)
        left top
        no-repeat
      `,
                input: 'password',
                inputPlaceholder: 'type admin code',
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                        if (value.length === 0) {
                            resolve('you must specify the administrator code');
                        }
                        else {
                            const admCode$ = this.wsService.listen('admCode')
                                .subscribe((data) => {
                                admCode$.unsubscribe();
                                resolve(!data.status ? 'wrong code' : '');
                            });
                            this.wsService.emmit('admCode', { code: value });
                        }
                    });
                }
            }).then((result) => {
                if (result.isConfirmed)
                    this.wsService.emmit('logFile');
            });
        });
    }
    systemResetListener(data) {
        //console.log(data);
        this.rtlModel = { proc: false, gainSelected: -1, speed: 4 };
        this.grgsmscannerModel = { proc: false, testMode: this.testModeParam };
        this.grgsmslivemoonModel = { proc: false, testMode: this.testModeParam };
        this.tsharkModel = {};
        this.inAction = false;
        this.systemResetModel.model = data;
        this.systemResetModel.proc = false;
    }
    rtlDeviceListener(data) {
        //console.log(data);
        var _a;
        this.rtlDeviceModel = data;
        this.rtlInUse = this.rtlDeviceModel.inUse;
        if (this.rtlDeviceModel.inUse) {
            if (this.rtlDeviceModel.service.find((qry) => qry.service == 'grgsm_scanner' || qry.service == 'rtl_test') != undefined) {
                $('.tableBtsSection, .btsMapzone').LoadingOverlay("show");
                this.grgsmscannerModel.proc = true;
                this.grgsmscannerModel.action = 1;
                return;
            }
            if (this.rtlDeviceModel.service.find((qry) => qry.service == 'grgsm_livemon') != undefined) {
                const rMod = this.rtlDeviceModel.service.find((qry) => qry.service == 'grgsm_livemon');
                if (!this.grgsmscannerModel.model) {
                    this.grgsmscannerModel.model = {
                        renderClient: true,
                        data: {
                            data: [rMod.data.btsInfo],
                            config: rMod.data.btsInfo.config
                        }
                    };
                    this.grgsmscannerModel.scanning = rMod.data.btsInfo;
                    setTimeout(() => {
                        $('.btnScannDo').addClass('d-none');
                        $('.btnScannCancel').removeClass('d-none');
                        $('.btnScannCancel').closest('tr').addClass('bg-inproc');
                        this.tsharkModel.model = null;
                    }, 200);
                }
                else {
                    const btsId = rMod.data.btsInfo.id;
                    const ele = $(`#btsId_` + btsId);
                    const btnScannDo = ele.find('.btnScannDo');
                    const btnScannCancel = ele.find('.btnScannCancel');
                    if (!((_a = this.grgsmscannerModel) === null || _a === void 0 ? void 0 : _a.model.renderClient) && btnScannCancel.hasClass("d-none")) {
                        btnScannDo.html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span><span> Scanning...</span>`);
                        btnScannCancel.removeClass('d-none');
                        ele.addClass('bg-inproc');
                        this.tsharkModel.model = null;
                    }
                }
                return;
            }
        }
        else {
            if (!this.rtlModel.model) {
                this.rtlGainsAction(null);
            }
        }
        $('.tableBtsSection, .btsMapzone').LoadingOverlay("hide", true);
    }
    rtlGainsAction(evt) {
        var _a, _b;
        if (!((_b = (_a = this.statusModel) === null || _a === void 0 ? void 0 : _a.model) === null || _b === void 0 ? void 0 : _b.data.radios.status))
            return;
        this.rtlInUse = true;
        this.inAction = true;
        this.rtlModel.proc = true;
        this.wsService.emmit('rtlGain', {});
    }
    rtlGainsListener(data) {
        //console.log(data);
        this.rtlModel.model = data;
        if (!this.rtlModel.model.status && this.rtlModel.model.message == "running process") {
            setTimeout(() => {
                this.rtlGainsAction(null);
            }, 1000);
            return;
        }
        this.rtlModel.proc = false;
        this.inAction = false;
        try {
            this.rtlModel.gainSelected = this.rtlModel.model.gainValues[this.rtlModel.model.gainValues.length - 1];
        }
        catch (error) { }
    }
    grgsmScannerAction(evt, action) {
        if (!this.statusModel.model.data.radios.status)
            return;
        this.inAction = true;
        this.grgsmscannerModel.proc = true;
        this.grgsmscannerModel.action = action;
        this.gsmVerbose.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
        switch (action) {
            case 1:
                this.grgsmscannerModel.model = null;
                this.grgsmscannerModel.scanning = null;
                this.tsharkModel = {};
                this.wsService.emmit('grgsmscanner', { gain: this.rtlModel.gainSelected, speed: this.rtlModel.speed, testMode: this.testModeParam });
                break;
            case 0:
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                    title: 'Confirm',
                    html: "cancel BTS's scanning",
                    icon: 'question',
                    showCancelButton: true
                }).then((result) => {
                    if (!result.isDismissed)
                        this.wsService.emmit('grgsmscannerKill', {});
                    else
                        this.grgsmscannerModel.action = 1;
                });
                break;
            default:
                break;
        }
    }
    grgsmScannerListener(data) {
        //console.log(data);
        //debugger;
        this.grgsmscannerModel.model = data;
        this.grgsmscannerModel.scanning = null;
        this.grgsmscannerModel.proc = false;
        this.inAction = false;
        this.btsTableSection.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
        webNotification.showNotification("BTS's scann", {
            body: "BTS's scanning process finished...",
            icon: 'favicon.ico',
            onClick: function onNotificationClicked() {
                parent.focus();
                window.focus();
            },
        }, function onShow(error, hide) {
            if (!error) {
                hide();
            }
        });
    }
    grgsmScannerKillListener(data) {
        //console.log(data);
        this.grgsmscannerModel.proc = false;
        this.grgsmscannerModel.action = null;
        this.inAction = false;
    }
    grgsmLivemonAction(btn, idBts, freq, action, btnObj, lineTr, btsInfo) {
        if (!this.statusModel.model.data.radios.status)
            return;
        this.inAction = true;
        this.grgsmslivemoonModel.proc = true;
        this.grgsmslivemoonModel.action = action;
        switch (action) {
            case 1:
                this.grgsmscannerModel.scanning = btsInfo;
                let __freq = freq.match(/\d/g);
                __freq = __freq.join("");
                while (__freq.length < 10)
                    __freq += "0";
                //TODO: Only TEST
                if (this.testModeParam == true) {
                    //this.grgsmscannerModel.model.data.config.gain = '42.0';
                    //__freq = '1950800000';
                }
                //debugger;
                this.wsService.emmit('grgsmlivemon', {
                    id: idBts,
                    freq: __freq,
                    gain: this.grgsmscannerModel.model.data.config.gain,
                    ppm: this.grgsmscannerModel.model.data.config.ppm,
                    btsInfo: btsInfo,
                    testMode: this.testModeParam
                });
                const dom = this.elementRef.nativeElement;
                const btnsScannDo = dom.querySelectorAll('.btnScannDo');
                btnsScannDo.forEach((item) => {
                    item.disabled = true;
                });
                btn.innerHTML =
                    `<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
        <span> Scanning...</span>`;
                btnObj.classList.remove('d-none');
                lineTr.classList.add('bg-inproc');
                this.tsharkModel.model = null;
                break;
            case 0:
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                    title: 'Confirm',
                    html: "cancel scanning",
                    icon: 'question',
                    showCancelButton: true
                }).then((result) => {
                    if (!result.isDismissed) {
                        this.wsService.emmit('grgsmlivemonKill', {});
                        //TODO: Only test
                        //this.grgsmLivemoonKillListener(null);
                        this.grgsmscannerModel.scanning = null;
                    }
                });
                break;
            default:
                break;
        }
    }
    grgsmLivemoonListener(data) {
        //console.log(data);
        this.grgsmslivemoonModel.model = data;
        this.grgsmslivemoonModel.proc = false;
        this.inAction = false;
    }
    grgsmLivemoonKillListener(data) {
        //debugger;
        //console.log(data);
        var _a;
        const dom = this.elementRef.nativeElement;
        const btnsScannDo = dom.querySelectorAll('.btnScannDo');
        const btnScannCancel = dom.querySelectorAll('.btnScannCancel');
        btnsScannDo.forEach((item) => {
            item.disabled = false;
            item.innerHTML = `Scan`;
        });
        btnScannCancel.forEach((item) => {
            item.classList.add("d-none");
        });
        $('table tbody tr.bg-inproc').removeClass('bg-inproc');
        this.grgsmslivemoonModel.proc = false;
        this.grgsmslivemoonModel.action = null;
        if ((_a = this.grgsmscannerModel) === null || _a === void 0 ? void 0 : _a.model.renderClient) {
            this.grgsmscannerModel.model = null;
            this.grgsmscannerModel.scanning = null;
        }
        this.inAction = false;
    }
    tsharkListener(data) {
        //debugger;
        //console.log(data);
        this.tsharkModel.model = data;
    }
    RTLCalibrateListener(data) {
        this.RTLCalibrateModel = data;
    }
    logFileListener(data) {
        if (data.status)
            this.downloadString(data.data.join(''), 'text/plain', data.file);
        else
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                html: `Log file [ ${data.file} ] not found`
            });
    }
    gainSelectChange(value) {
        this.rtlModel.gainSelected = +value;
    }
    speedSelectChange(value) {
        this.rtlModel.speed = +value;
    }
    mapCenter(cords, zoom) {
        //debugger;
        if (this.mapModel.dragCors.drageed) {
            this.mapModel.mapCenterCors.lat = this.mapModel.dragCors.lat;
            this.mapModel.mapCenterCors.lng = this.mapModel.dragCors.lng;
            this.mapModel.dragCors.drageed = false;
        }
        setTimeout(() => {
            this.mapModel.mapCenterCors = {
                lat: Number(cords.lat),
                lng: Number(cords.lng)
            };
            this.mapModel.zoom = zoom;
        }, 0);
    }
    mapZoomChange(zoom) {
        this.mapModel.zoom = zoom;
    }
    mapCenterChange(latLng) {
        this.mapModel.dragCors = {
            drageed: true,
            lat: latLng.lat,
            lng: latLng.lng
        };
    }
    verticalTablesDragSplit(data) {
    }
    downloadString(text, fileType, fileName) {
        var blob = new Blob([text], { type: fileType });
        var a = document.createElement('a');
        a.download = fileName;
        a.href = URL.createObjectURL(blob);
        a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(function () { URL.revokeObjectURL(a.href); }, 1500);
    }
    sessionSwal() {
        var _a, _b;
        if (!this.wsService.socketStatus)
            return;
        if (!this.sessionSwalInterval) {
            this.sessionSwalInterval = setInterval(() => {
                if (!sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.isVisible() && !this.wsService.authStatus) {
                    this.sessionSwal();
                }
            }, 10);
        }
        if (!sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.isVisible() && !this.wsService.authStatus && !this.wsService.getSessCookie()) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
                allowOutsideClick: false,
                allowEscapeKey: false,
                backdrop: `
          rgba(0,0,0,0.97)
          left top
          no-repeat
        `,
                title: 'Access',
                icon: 'info',
                showCancelButton: true,
                cancelButtonColor: 'red',
                confirmButtonText: `Go in`,
                cancelButtonText: 'Shutdown IMSI computer',
                input: 'password',
                inputPlaceholder: 'type admin code',
                inputValidator: (value) => this.wsService.auth(value),
                footer: this.projectVersions ?
                    `<div class='w-100 text-right' style='font-size: 13px;'>
            <i class="fa fa-code-fork pr-1" aria-hidden="true"></i> [ imsi &nbsp;<strong>${(_a = this.projectVersions) === null || _a === void 0 ? void 0 : _a.imsi}</strong> &nbsp; | &nbsp; ws &nbsp; <strong>${(_b = this.projectVersions) === null || _b === void 0 ? void 0 : _b.ws}</strong> &nbsp; ]
          </div>` : ''
            }).then((result) => {
                if (!result.isConfirmed) {
                    if (this.wsService.socketStatus) {
                        this.eventIMSIComputer(null, 1);
                    }
                }
            });
        }
    }
};
MainComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__["CookieService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ElementRef"] },
    { type: src_app_services_websocket_service__WEBPACK_IMPORTED_MODULE_8__["WebsocketService"] },
    { type: ng_connection_service__WEBPACK_IMPORTED_MODULE_6__["ConnectionService"] }
];
MainComponent.propDecorators = {
    gsmVerbose: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"], args: ["gsmVerbose",] }],
    btsTableSection: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"], args: ["btsTableSection",] }],
    beforeUnloadHandler: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["HostListener"], args: ['window:beforeunload', ['$event'],] }]
};
MainComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-main',
        template: _raw_loader_main_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_main_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MainComponent);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/imsi/imsigular/src/main.ts */"zUnb");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ "18sH":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/shared/only-one-instance/only-one-instance.component.ts ***!
  \*******************************************************************************/
/*! exports provided: OnlyOneInstanceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnlyOneInstanceComponent", function() { return OnlyOneInstanceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_only_one_instance_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./only-one-instance.component.html */ "ygYd");
/* harmony import */ var _only_one_instance_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./only-one-instance.component.scss */ "Jp2W");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");




let OnlyOneInstanceComponent = class OnlyOneInstanceComponent {
    constructor() { }
    ngOnInit() {
    }
};
OnlyOneInstanceComponent.ctorParameters = () => [];
OnlyOneInstanceComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-only-one-instance',
        template: _raw_loader_only_one_instance_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_only_one_instance_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], OnlyOneInstanceComponent);



/***/ }),

/***/ "7iTK":
/*!******************************************!*\
  !*** ./src/app/pipes/filter-mnc.pipe.ts ***!
  \******************************************/
/*! exports provided: FilterMNCPipe, filterBtsScanningPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterMNCPipe", function() { return FilterMNCPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterBtsScanningPipe", function() { return filterBtsScanningPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");


let FilterMNCPipe = class FilterMNCPipe {
    transform(model, MNCVal) {
        if (!model)
            return [];
        return model.filter((fil) => fil.MNC == MNCVal);
    }
};
FilterMNCPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filterMNC'
    })
], FilterMNCPipe);

let filterBtsScanningPipe = class filterBtsScanningPipe {
    transform(model, btsScanningInfo) {
        if (btsScanningInfo && model.length > 0) {
            return model.filter(fil => fil.ARFCN != btsScanningInfo.ARFCN && fil.Freq != btsScanningInfo.Freq && fil.CID != btsScanningInfo.CID);
        }
        return model;
    }
};
filterBtsScanningPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filterBtsScanning'
    })
], filterBtsScanningPipe);



/***/ }),

/***/ "AXJl":
/*!**************************************************************!*\
  !*** ./src/app/pages/shared/errs/err404/err404.component.ts ***!
  \**************************************************************/
/*! exports provided: Err404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Err404Component", function() { return Err404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_err404_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./err404.component.html */ "+7GD");
/* harmony import */ var _err404_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./err404.component.scss */ "G/Rb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");




let Err404Component = class Err404Component {
    constructor() { }
    ngOnInit() {
    }
};
Err404Component.ctorParameters = () => [];
Err404Component = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-err404',
        template: _raw_loader_err404_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_err404_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Err404Component);



/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: false,
    version: '1.12052004.03012020',
    socket: {
        host: 'https://192.168.0.200',
        port: 8080,
        namespace: 'imsi'
    },
    googleMap: {
        apiKey: 'AIzaSyCHaLK4OOwWGXwahw_iH8wq2cuUis50cnc'
    },
    cookieImsiInstanceName: 'imsiInstance'
};


/***/ }),

/***/ "FGth":
/*!***********************************************************!*\
  !*** ./src/app/pages/shared/footer/footer.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb290ZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "G/Rb":
/*!****************************************************************!*\
  !*** ./src/app/pages/shared/errs/err404/err404.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlcnI0MDQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "GaY0":
/*!***********************************************************!*\
  !*** ./src/app/pages/shared/header/header.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "Gyf/":
/*!***********************************************!*\
  !*** ./src/app/services/websocket.service.ts ***!
  \***********************************************/
/*! exports provided: WebsocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebsocketService", function() { return WebsocketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "b6Qw");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../environments/environment */ "AytR");






let WebsocketService = class WebsocketService {
    constructor(cookieService) {
        this.cookieService = cookieService;
        this.socket = null;
        this.socketStatus = false;
        this.socketConnected = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.authStatusSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.authStatus = false;
        this.authSubs$ = null;
        this.intervalAuth = null;
        this.sid = 'authSess';
        this.projectVersions = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.socket = io.connect(`${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].socket.host}:${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].socket.port}/${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].socket.namespace}`);
        this.socket.on('connect', () => {
            this.socketStatus = true;
            this.socketConnected.next(this.socketStatus);
            if (!this.authSubs$) {
                this.authSubs$ = this.listen('authCheck')
                    .subscribe((data) => {
                    this.authStatus = data.status;
                    this.authStatusSub.next(data.status);
                    if (!this.intervalAuth) {
                        this.intervalAuth = setInterval(() => {
                            //clearInterval(this.intervalAuth);
                            //TODO: get jwtoken from cookie
                            const jwToken = this.cookieService.get(this.sid);
                            this.emmit('authCheck', { jwToken: jwToken });
                        }, 1000);
                    }
                });
            }
        });
        this.socket.on('disconnect', () => {
            this.socketStatus = false;
            this.socketConnected.next(this.socketStatus);
            clearInterval(this.intervalAuth);
            this.intervalAuth = null;
            this.authSubs$.unsubscribe();
            this.authSubs$ = null;
            this.authStatus = false;
            this.delSessCookie();
        });
        this.listen('status')
            .subscribe((data) => {
            this.projectVersions.next({
                imsi: `v${_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].version}`,
                ws: `v${data.data.wsVersion}`,
            });
        });
    }
    auth(admCode) {
        return new Promise((resolve) => {
            const auth$ = this.listen('auth')
                .subscribe((data) => {
                auth$.unsubscribe();
                if (data.status) {
                    //TODO: save jwToken to cokie [ data.jwToken ]
                    this.cookieService.set(this.sid, data.jwToken);
                    this.emmit('authCheck', { jwToken: data.jwToken });
                }
                resolve(!data.status ? 'wrong code' : '');
            });
            this.emmit('auth', { code: admCode });
        });
    }
    delSessCookie() {
        this.cookieService.delete(this.sid);
    }
    getSessCookie() {
        return this.cookieService.get(this.sid);
    }
    emmit(action, payload) {
        this.socket.emit(action, payload);
    }
    listen(action) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["fromEvent"])(this.socket, action)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((data) => {
            return data;
        }));
    }
};
WebsocketService.ctorParameters = () => [
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] }
];
WebsocketService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], WebsocketService);



/***/ }),

/***/ "Jp2W":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/shared/only-one-instance/only-one-instance.component.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJvbmx5LW9uZS1pbnN0YW5jZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "Oznm":
/*!*********************************************************!*\
  !*** ./src/app/pages/shared/header/header.component.ts ***!
  \*********************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./header.component.html */ "sUGB");
/* harmony import */ var _header_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header.component.scss */ "GaY0");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var src_app_services_websocket_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/websocket.service */ "Gyf/");





let HeaderComponent = class HeaderComponent {
    constructor(wsService) {
        this.wsService = wsService;
        this.projectVersions = null;
    }
    ngOnInit() {
        this.wsService.projectVersions.subscribe((data) => {
            this.projectVersions = { imsi: data.imsi, ws: data.ws };
        });
    }
};
HeaderComponent.ctorParameters = () => [
    { type: src_app_services_websocket_service__WEBPACK_IMPORTED_MODULE_4__["WebsocketService"] }
];
HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HeaderComponent);



/***/ }),

/***/ "QM7E":
/*!************************************************!*\
  !*** ./src/app/pages/main/main.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".verbose {\n  font-size: 14px;\n}\n\nagm-map {\n  height: 100%;\n  width: 100%;\n}\n\n.tableBtsSection {\n  min-height: 600px !important;\n}\n\n.bg-inproc {\n  background-color: #420000 !important;\n}\n\n.btsTableScroll {\n  display: block;\n  overflow-y: scroll;\n  width: 100%;\n}\n\n.targetsTableScroll {\n  height: calc(100% - 50px);\n  display: block;\n  overflow-y: scroll;\n  width: 100%;\n  margin: 0;\n}\n\n.align-super {\n  vertical-align: super !important;\n}\n\n.table thead th {\n  border-bottom: none;\n}\n\nhr {\n  border-top: 1px solid rgba(0, 0, 0, 0.77);\n}\n\n::ng-deep .as-split-gutter {\n  background-color: transparent !important;\n}\n\n.ribbon-wrapper {\n  width: 128px;\n  height: 128px;\n  overflow: hidden;\n  position: absolute;\n  top: -3px;\n  left: -3px;\n}\n\n.ribbon {\n  font-size: 12px;\n  color: #FFF;\n  text-transform: uppercase;\n  font-family: \"Montserrat Bold\", \"Helvetica Neue\", Helvetica, Arial, sans-serif;\n  letter-spacing: 0.05em;\n  line-height: 15px;\n  text-align: center;\n  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);\n  transform: rotate(-45deg);\n  position: relative;\n  padding: 7px 0;\n  left: -25px;\n  top: 23px;\n  width: 128px;\n  height: 28px;\n  box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);\n  background-color: #dedede;\n  background-image: linear-gradient(to bottom, #ffffff 45%, #dedede 100%);\n  background-repeat: repeat-x;\n  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#ffffffff\", endColorstr=\"#ffdedede\", GradientType=0);\n}\n\n.ribbon:before,\n.ribbon:after {\n  content: \"\";\n  border-top: 3px solid #9e9e9e;\n  border-left: 3px solid transparent;\n  border-right: 3px solid transparent;\n  position: absolute;\n  bottom: -3px;\n}\n\n.ribbon:before {\n  left: 0;\n}\n\n.ribbon:after {\n  right: 0;\n}\n\n.ribbon.green {\n  background-color: #2da285;\n  background-image: linear-gradient(to bottom, #2da285 45%, #227a64 100%);\n  background-repeat: repeat-x;\n  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#ff2da285\", endColorstr=\"#ff227a64\", GradientType=0);\n}\n\n.ribbon.green:before,\n.ribbon.green:after {\n  border-top: 3px solid #113e33;\n}\n\n.ribbon.red {\n  background-color: #bc1a3a;\n  background-image: linear-gradient(to bottom, #a61733 45%, #bc1a3a 100%);\n  background-repeat: repeat-x;\n  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#ffa61733\", endColorstr=\"#ffbc1a3a\", GradientType=0);\n}\n\n.ribbon.red:before,\n.ribbon.red:after {\n  border-top: 3px solid #8f142c;\n}\n\n.ribbon.blue {\n  background-color: #1a8bbc;\n  background-image: linear-gradient(to bottom, #177aa6 45%, #1a8bbc 100%);\n  background-repeat: repeat-x;\n  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#177aa6\", endColorstr=\"#ff1a8bbc\", GradientType=0);\n}\n\n.ribbon.blue:before,\n.ribbon.blue:after {\n  border-top: 3px solid #115979;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21haW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsV0FBQTtBQUVGOztBQUFBO0VBQ0UsNEJBQUE7QUFHRjs7QUFBQTtFQUNFLG9DQUFBO0FBR0Y7O0FBQUE7RUFHRSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBRUE7RUFHRSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBREY7O0FBSUE7RUFDRSxnQ0FBQTtBQURGOztBQUlBO0VBQ0UsbUJBQUE7QUFERjs7QUFJQTtFQUNFLHlDQUFBO0FBREY7O0FBSUE7RUFDRSx3Q0FBQTtBQURGOztBQUtBO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFGRjs7QUFLQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSw4RUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHdDQUFBO0VBS0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsc0NBQUE7RUFDQSx5QkFBQTtFQUdBLHVFQUFBO0VBQ0EsMkJBQUE7RUFDQSxzSEFBQTtBQUZGOztBQUtBOztFQUVFLFdBQUE7RUFDQSw2QkFBQTtFQUNBLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFGRjs7QUFLQTtFQUNFLE9BQUE7QUFGRjs7QUFLQTtFQUNFLFFBQUE7QUFGRjs7QUFNQTtFQUNFLHlCQUFBO0VBR0EsdUVBQUE7RUFDQSwyQkFBQTtFQUNBLHNIQUFBO0FBSEY7O0FBTUE7O0VBRUUsNkJBQUE7QUFIRjs7QUFNQTtFQUNFLHlCQUFBO0VBR0EsdUVBQUE7RUFDQSwyQkFBQTtFQUNBLHNIQUFBO0FBSEY7O0FBTUE7O0VBRUUsNkJBQUE7QUFIRjs7QUFPQTtFQUNFLHlCQUFBO0VBR0EsdUVBQUE7RUFDQSwyQkFBQTtFQUNBLG9IQUFBO0FBSkY7O0FBT0E7O0VBRUUsNkJBQUE7QUFKRiIsImZpbGUiOiJtYWluLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnZlcmJvc2V7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbmFnbS1tYXAge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnRhYmxlQnRzU2VjdGlvbiB7XG4gIG1pbi1oZWlnaHQ6IDYwMHB4IWltcG9ydGFudDtcbn1cblxuLmJnLWlucHJvYyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjAwMDAhaW1wb3J0YW50O1xufVxuXG4uYnRzVGFibGVTY3JvbGwge1xuICAvL21heC1oZWlnaHQ6MjMwcHg7XG4gIC8vaGVpZ2h0OjIzMHB4O1xuICBkaXNwbGF5OmJsb2NrO1xuICBvdmVyZmxvdy15OnNjcm9sbDtcbiAgd2lkdGg6MTAwJTtcbn1cblxuLnRhcmdldHNUYWJsZVNjcm9sbHtcbiAgLy9tYXgtaGVpZ2h0Ojg3JTtcbiAgLy9oZWlnaHQ6ODclO1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDUwcHgpO1xuICBkaXNwbGF5OmJsb2NrO1xuICBvdmVyZmxvdy15OnNjcm9sbDtcbiAgd2lkdGg6MTAwJTtcbiAgbWFyZ2luOiAwO1xufVxuXG4uYWxpZ24tc3VwZXIge1xuICB2ZXJ0aWNhbC1hbGlnbjogc3VwZXIhaW1wb3J0YW50O1xufVxuXG4udGFibGUgdGhlYWQgdGgge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuXG5ociB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2IoMCAwIDAgLyA3NyUpO1xufVxuXG46Om5nLWRlZXAgLmFzLXNwbGl0LWd1dHRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG5cblxuLnJpYmJvbi13cmFwcGVyIHtcbiAgd2lkdGg6IDEyOHB4O1xuICBoZWlnaHQ6IDEyOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTNweDtcbiAgbGVmdDogLTNweFxufVxuXG4ucmliYm9uIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogI0ZGRjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0IEJvbGQnLCAnSGVsdmV0aWNhIE5ldWUnLCBIZWx2ZXRpY2EsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICBsZXR0ZXItc3BhY2luZzogLjA1ZW07XG4gIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtc2hhZG93OiAwIC0xcHggMCByZ2JhKDAsIDAsIDAsIC40KTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICAtbW96LXRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICAtby10cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmc6IDdweCAwO1xuICBsZWZ0OiAtMjVweDtcbiAgdG9wOiAyM3B4O1xuICB3aWR0aDogMTI4cHg7XG4gIGhlaWdodDogMjhweDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgM3B4IHJnYmEoMCwgMCwgMCwgLjMpO1xuICBib3gtc2hhZG93OiAwIDAgM3B4IHJnYmEoMCwgMCwgMCwgLjMpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGVkZWRlO1xuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYgNDUlLCAjZGVkZWRlIDEwMCUpO1xuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQodG9wLCAjZmZmZmZmIDQ1JSwgI2RlZGVkZSAxMDAlKTtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2ZmZmZmZiA0NSUsICNkZWRlZGUgMTAwJSk7XG4gIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgZmlsdGVyOiBwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuZ3JhZGllbnQoc3RhcnRDb2xvcnN0cj0nI2ZmZmZmZmZmJywgZW5kQ29sb3JzdHI9JyNmZmRlZGVkZScsIEdyYWRpZW50VHlwZT0wKVxufVxuXG4ucmliYm9uOmJlZm9yZSxcbi5yaWJib246YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBib3JkZXItdG9wOiAzcHggc29saWQgIzllOWU5ZTtcbiAgYm9yZGVyLWxlZnQ6IDNweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLXJpZ2h0OiAzcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAtM3B4XG59XG5cbi5yaWJib246YmVmb3JlIHtcbiAgbGVmdDogMFxufVxuXG4ucmliYm9uOmFmdGVyIHtcbiAgcmlnaHQ6IDBcbn1cblxuXG4ucmliYm9uLmdyZWVuIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJkYTI4NTtcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjMmRhMjg1IDQ1JSwgIzIyN2E2NCAxMDAlKTtcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KHRvcCwgIzJkYTI4NSA0NSUsICMyMjdhNjQgMTAwJSk7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sICMyZGEyODUgNDUlLCAjMjI3YTY0IDEwMCUpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGZpbHRlcjogcHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LmdyYWRpZW50KHN0YXJ0Q29sb3JzdHI9JyNmZjJkYTI4NScsIGVuZENvbG9yc3RyPScjZmYyMjdhNjQnLCBHcmFkaWVudFR5cGU9MClcbn1cblxuLnJpYmJvbi5ncmVlbjpiZWZvcmUsXG4ucmliYm9uLmdyZWVuOmFmdGVyIHtcbiAgYm9yZGVyLXRvcDogM3B4IHNvbGlkICMxMTNlMzNcbn1cblxuLnJpYmJvbi5yZWQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYmMxYTNhO1xuICBiYWNrZ3JvdW5kLWltYWdlOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNhNjE3MzMgNDUlLCAjYmMxYTNhIDEwMCUpO1xuICBiYWNrZ3JvdW5kLWltYWdlOiAtby1saW5lYXItZ3JhZGllbnQodG9wLCAjYTYxNzMzIDQ1JSwgI2JjMWEzYSAxMDAlKTtcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2E2MTczMyA0NSUsICNiYzFhM2EgMTAwJSk7XG4gIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgZmlsdGVyOiBwcm9naWQ6RFhJbWFnZVRyYW5zZm9ybS5NaWNyb3NvZnQuZ3JhZGllbnQoc3RhcnRDb2xvcnN0cj0nI2ZmYTYxNzMzJywgZW5kQ29sb3JzdHI9JyNmZmJjMWEzYScsIEdyYWRpZW50VHlwZT0wKVxufVxuXG4ucmliYm9uLnJlZDpiZWZvcmUsXG4ucmliYm9uLnJlZDphZnRlciB7XG4gIGJvcmRlci10b3A6IDNweCBzb2xpZCAjOGYxNDJjXG59XG5cblxuLnJpYmJvbi5ibHVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFhOGJiYztcbiAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjMTc3YWE2IDQ1JSwgIzFhOGJiYyAxMDAlKTtcbiAgYmFja2dyb3VuZC1pbWFnZTogLW8tbGluZWFyLWdyYWRpZW50KHRvcCwgIzE3N2FhNiA0NSUsICMxYThiYmMgMTAwJSk7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sICMxNzdhYTYgNDUlLCAjMWE4YmJjIDEwMCUpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGZpbHRlcjogcHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LmdyYWRpZW50KHN0YXJ0Q29sb3JzdHI9JyMxNzdhYTYnLCBlbmRDb2xvcnN0cj0nI2ZmMWE4YmJjJywgR3JhZGllbnRUeXBlPTApXG59XG5cbi5yaWJib24uYmx1ZTpiZWZvcmUsXG4ucmliYm9uLmJsdWU6YWZ0ZXIge1xuICBib3JkZXItdG9wOiAzcHggc29saWQgIzExNTk3OVxufVxuIl19 */");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");




let AppComponent = class AppComponent {
    constructor() {
        this.title = 'IMSI CATCHER';
    }
};
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header></app-header>\n<router-outlet></router-outlet>\n<app-footer></app-footer>");

/***/ }),

/***/ "WjsP":
/*!*****************************************!*\
  !*** ./src/app/pipes/parse-rtl.pipe.ts ***!
  \*****************************************/
/*! exports provided: ParseRTLPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParseRTLPipe", function() { return ParseRTLPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");


let ParseRTLPipe = class ParseRTLPipe {
    transform(value) {
        return value.replace("rtl", "gsm").replace('grgsm', "gsm").toUpperCase();
    }
};
ParseRTLPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'parseRTL'
    })
], ParseRTLPipe);



/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "cUpR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-socket-io */ "RH9n");
/* harmony import */ var _pages_page_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/page.module */ "+jLx");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _pages_shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/shared/footer/footer.component */ "hNQr");
/* harmony import */ var _pages_shared_header_header_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/shared/header/header.component */ "Oznm");
/* harmony import */ var _pages_shared_only_one_instance_only_one_instance_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/shared/only-one-instance/only-one-instance.component */ "18sH");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-cookie-service */ "b6Qw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "iInd");














const config = {
    url: `${src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].socket.host}:${src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].socket.port}`,
    options: {}
};
let AppModule = class AppModule {
    constructor(router, cookieService) {
        this.router = router;
        this.cookieService = cookieService;
        if (!this.cookieService.check(src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].cookieImsiInstanceName))
            this.cookieService.set(src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].cookieImsiInstanceName, '1');
        else {
            this.router.navigate(['/singleInstance']);
        }
    }
};
AppModule.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_13__["Router"] },
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_12__["CookieService"] }
];
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
            _pages_shared_header_header_component__WEBPACK_IMPORTED_MODULE_10__["HeaderComponent"],
            _pages_shared_footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"],
            _pages_shared_only_one_instance_only_one_instance_component__WEBPACK_IMPORTED_MODULE_11__["OnlyOneInstanceComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            ngx_socket_io__WEBPACK_IMPORTED_MODULE_5__["SocketIoModule"].forRoot(config),
            _pages_page_module__WEBPACK_IMPORTED_MODULE_6__["PageModule"]
        ],
        providers: [
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_12__["CookieService"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "bBbC":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/main.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"testModeParam == true\" class=\"ribbon-wrapper\">\n  <div class=\"ribbon red\">Test Mode</div>\n</div>\n\n\n<div class=\"\" style=\"height: 99%!Important;\">\n  <div class=\"row align-items-center h-100 m-0\">\n      <div *ngIf=\"!wsService.socketStatus\" class=\"col-12 mx-auto text-center\">\n          <h1>\n            IMSI SERVER [\n            <strong class=\"text-danger\"><i class=\"fa fa-chain-broken animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i> DISCONNECTED</strong>\n            ]\n          </h1>\n          <h6 *ngIf=\"!wsService.socketStatus\" class=\"text-warning\">TRYING TO CONNECT... <i class=\"fa fa-cog fa-spin fa-fw text-white\"></i></h6>\n\n          <!--\n          <div class=\"row align-items-center justify-content-center mt-5\">\n            <button type=\"button\" class=\"col-* btn btn-outline-danger \" (click)='eventIMSIComputer($event,0)'><i class=\"fa fa-refresh\" aria-hidden=\"true\"></i> Restart IMSI computer</button>\n            <button type=\"button\" class=\"col-* btn btn-outline-danger ml-4\" (click)='eventIMSIComputer($event,1)'><i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> Shutdown IMSI computer</button>\n          </div>\n          -->\n      </div>\n\n\n    <div *ngIf=\"statusModel.model && wsService.socketStatus\" class=\"pt-3 col-12 animate__animated animate__fadeIn\">\n\n      <div class=\"mx-auto row align-items-center px-5 text-center\">\n        <div class=\"col\">\n          <h4 class=\"border-bottom py-1\"><i class=\"fa fa-database animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i><span class=\"d-none d-xl-inline\"> Database</span></h4>\n          <p [ngClass]=\"{'text-center': true, 'font-weight-bold': true, 'text-success': statusModel.model.data.db.status, 'text-danger': !statusModel.model.data.db.status}\">\n            <i *ngIf=\"statusModel.model.data.db.status\" class=\"fa fa-thumbs-o-up fa-2x text-success\" aria-hidden=\"true\"></i>\n            <i *ngIf=\"!statusModel.model.data.db.status\" class=\"fa fa-thumbs-o-down fa-2x text-danger\" aria-hidden=\"true\"></i>\n            <span class=\"pl-2 align-super\">\n              [ {{ statusModel.model.data.db.status ? 'ACTIVE' : 'INACTIVE' }} ]\n            </span>\n          </p>\n        </div>\n        <div class=\"col\">\n          <h4 class=\"border-bottom py-1\"><i class=\"fa fa-wifi animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i><span class=\"d-none d-xl-inline\"> Internet Server</span></h4>\n          <p [ngClass]=\"{'text-center': true, 'font-weight-bold': true, 'text-success': statusModel.model.data.net.status, 'text-danger': !statusModel.model.data.net.status}\">\n            <i *ngIf=\"statusModel.model.data.net.status\" class=\"fa fa-thumbs-o-up fa-2x text-success\" aria-hidden=\"true\"></i>\n            <i *ngIf=\"!statusModel.model.data.net.status\" class=\"fa fa-thumbs-o-down fa-2x text-danger\" aria-hidden=\"true\"></i>\n            <span class=\"pl-2 align-super\">\n              [ {{ statusModel.model.data.net.status ? 'ACTIVE' : 'INACTIVE' }} ]\n            </span>\n          </p>\n        </div>\n        <div class=\"col\">\n          <h4 class=\"border-bottom py-1\"><i class=\"fa fa-wifi animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i><span class=\"d-none d-xl-inline\"> Internet in Browser</span></h4>\n          <p [ngClass]=\"{'text-center': true, 'font-weight-bold': true, 'text-success': withInternet, 'text-danger': !withInternet}\">\n            <i *ngIf=\"withInternet\" class=\"fa fa-thumbs-o-up fa-2x text-success\" aria-hidden=\"true\"></i>\n            <i *ngIf=\"!withInternet\" class=\"fa fa-thumbs-o-down fa-2x text-danger\" aria-hidden=\"true\"></i>\n            <span class=\"pl-2 align-super\">\n              [ {{ withInternet ? 'ACTIVE' : 'INACTIVE' }} ]\n            </span>\n          </p>\n        </div>\n        <div class=\"col\">\n          <h4 class=\"border-bottom py-1\"><i class=\"fa fa-usb animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i><span class=\"d-none d-xl-inline\"> GSM Device</span></h4>\n          <p [ngClass]=\"{'text-center': true, 'font-weight-bold': true, 'text-success': statusModel.model.data.radios.status, 'text-danger': !statusModel.model.data.radios.status}\">\n            <i *ngIf=\"statusModel.model.data.radios.status\" class=\"fa fa-thumbs-o-up fa-2x text-success\" aria-hidden=\"true\"></i>\n            <i *ngIf=\"!statusModel.model.data.radios.status\" class=\"fa fa-thumbs-o-down fa-2x text-danger\" aria-hidden=\"true\"></i>\n            <span class=\"pl-2 align-super\">\n              [ {{ statusModel.model.data.radios.status ? 'CONNECTED' : 'DISCONNECTED' }} ]\n            </span>\n          </p>\n        </div>\n        <div class=\"col\">\n          <h4 class=\"border-bottom py-1\"><i class=\"fa fa-eye animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i><span class=\"d-none d-xl-inline\"> GSM Free</span></h4>\n          <p class=\"text-center\" [ngClass]=\"{'font-weight-bold': true, 'text-success': !rtlInUse || !inAction || statusModel.model.data.radios.status, 'text-danger': rtlInUse || inAction || !statusModel.model.data.radios.status}\">\n            <i *ngIf=\"!rtlInUse && !inAction && statusModel.model.data.radios.status\" class=\"fa fa-unlock-alt fa-2x text-success animate__animated animate__infinite animate__headShake\" aria-hidden=\"true\"></i>\n            <i *ngIf=\"rtlInUse || inAction || !statusModel.model.data.radios.status\" class=\"fa fa-lock fa-2x text-danger animate__animated animate__infinite animate__headShake\" aria-hidden=\"true\"></i>\n            <span class=\"pl-2 align-super\">\n              [ {{ !rtlInUse && !inAction && statusModel.model.data.radios.status ? 'TRUE' : 'FALSE' }} ]\n            </span>\n          </p>\n        </div>\n        <div class=\"col-3 border-left text-right\">\n          <div class=\"row m-0\">\n\n            <div class=\"col\">\n              <div class=\"row\">\n                <button type=\"button\" class=\"col-12 btn btn-outline-success mb-2\" (click)='statusAction($event)'><i class=\"fa fa-refresh\" aria-hidden=\"true\"></i> Refresh status</button>\n                <button type=\"button\" class=\"col btn btn-outline-danger\" (click)='systemResetAction($event)'><i class=\"fa fa-cog\" aria-hidden=\"true\"></i> System reset</button>\n                <button type=\"button\" class=\"col-* btn btn-outline-warning ml-2\" (click)='getLogFile($event)' title=\"Download log file\"><i class=\"fa fa-file-text\" aria-hidden=\"true\"></i></button>\n              </div>\n            </div>\n\n            <button type=\"button\" class=\"col btn btn-outline-secondary ml-2\" (click)='signOut($event)'><i class=\"fa fa-sign-out fa-3x\" style=\" vertical-align: middle;\" aria-hidden=\"true\"></i> Sign off</button>\n\n            <div class=\"col-12 mt-2\">\n              <div class=\"row\">\n                <button type=\"button\" class=\"col btn btn-outline-danger \" (click)='eventIMSIComputer($event,0)'><i class=\"fa fa-refresh\" aria-hidden=\"true\"></i> Restart IMSI computer</button>\n                <button type=\"button\" class=\"col btn btn-outline-danger ml-2\" (click)='eventIMSIComputer($event,1)'><i class=\"fa fa-power-off\" aria-hidden=\"true\"></i> Shutdown IMSI computer</button>\n              </div>\n            </div>\n\n          </div>\n        </div>\n        <div #gsmVerbose class=\"col-12 text-left\" style='min-height:5rem!important;'>\n          <h6 class=\"border-bottom border-secondary text-info\">GSM Verbose</h6>\n          <div *ngIf=\"statusModel.model.data.radios.status\">\n            <span *ngIf=\"RTLCalibrateModel && RTLCalibrateModel?.status\" [ngClass]=\"{'text-white': true, 'verbose': true, 'pl-4': true, 'd-block': true }\" >\n              <i class=\"fa fa-cog fa-spin fa-fw\"></i> - <strong class=\"text-warning\">PPM Calibrate</strong> [ {{ RTLCalibrateModel.PPM }} ]\n            </span>\n            <span *ngIf=\"!RTLCalibrateModel || !RTLCalibrateModel?.status\" [ngClass]=\"{'text-white': true, 'verbose': true, 'pl-4': true, 'd-block': true }\" >\n              <i class=\"fa fa-cog fa-spin fa-fw\"></i> - <strong class=\"text-warning\">PPM Calibrating...</strong>\n            </span>\n\n            <span [ngClass]=\"{'text-white': true, 'verbose': true, 'pl-4': true, 'd-block': true }\" *ngFor=\"let verbose of (rtlDeviceModel ? rtlDeviceModel.service : [])\" >\n              <span *ngIf=\"verbose.service\">\n                <i class=\"fa fa-spinner fa-pulse fa-fw\"></i> - <strong class=\"text-info\">{{ verbose.service | parseRTL }}</strong> - {{ verbose.message | parseRTL }}\n              </span>\n            </span>\n          </div>\n          <div *ngIf=\"!statusModel.model.data.radios.status\">\n            <span [ngClass]=\"{'text-danger': true, 'pl-4': true, 'd-block': true }\" >\n              <i class=\"fa fa-exclamation-triangle text-white fa-fw\"></i> - <strong class=\"text-danger\">GSM Device Disconnected</strong>\n            </span>\n          </div>\n        </div>\n      </div>\n\n      <hr class=\"py-2 mx-4\">\n\n      <div *ngIf=\"!rtlDeviceModel\" class=\"container mx-auto text-center animate__animated animate__fadeIn\">\n        <h1>\n          <i class=\"fa fa-cog fa-spin fa-fw\"></i>\n          STARTING SYSTEM\n        </h1>\n      </div>\n\n      <div id='mainSection' *ngIf=\"(wsService.socketStatus || statusModel.model.status) && rtlDeviceModel\" class=\"animate__animated animate__fadeIn\">\n\n        <div class=\"container mx-auto row align-items-end border rounded p-2\">\n\n          <div class=\"col-4\">\n            <h4 class=\"d-block\">Gain's</h4>\n            <div class=\"input-group\">\n              <select #gainSelect class=\"custom-select\" (change)=\"gainSelectChange(gainSelect.value)\" [disabled]=\"inAction || rtlInUse || !statusModel.model.data.radios.status\">\n                <option *ngFor=\"let gain of (rtlModel.model ? rtlModel.model.gainValues : []); let i = index\" [value]='gain' [selected]=' i == (rtlModel.model ? rtlModel.model.gainValues.length -1 : 0) ' >\n                  {{gain}}\n                </option>\n              </select>\n              <div class=\"input-group-append\">\n                <button class=\"btn btn-outline-success\" type=\"button\" (click)='rtlGainsAction($event)' [disabled]=\"inAction || rtlInUse || !statusModel.model.data.radios.status\">\n                  <span *ngIf=\"!rtlModel.proc\">Refresh GSM Gains</span>\n                  <span *ngIf=\"rtlModel.proc\" class=\"spinner-grow spinner-grow-sm\" role=\"status\" aria-hidden=\"true\"></span>\n                  <span *ngIf=\"rtlModel.proc\"> Getting GSM Gains...</span>\n                </button>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-4\">\n            <h4 class=\"d-block\">Speed</h4>\n            <select #speedSelect class=\"custom-select\" (change)=\"speedSelectChange(speedSelect.value)\" [disabled]=\"inAction || rtlInUse || !statusModel.model.data.radios.status\">\n              <option *ngFor=\"let speed of [1,2,3,4,5]; let i = index\" [value]='speed' [selected]=' i == 3 ' >\n                {{speed}}\n              </option>\n            </select>\n          </div>\n          <div class=\"col-4 text-center\">\n            <div class=\"row p-0 m-0\">\n              <button class=\"col btn btn-outline-success mx-1\" type=\"button\" (click)='grgsmScannerAction($event,1)' [disabled]=\"inAction || rtlInUse || !statusModel.model.data.radios.status || !( (RTLCalibrateModel?.status || false) || testModeParam )\">\n                <span *ngIf=\"grgsmscannerModel.proc || !(RTLCalibrateModel?.status || false)\" class=\"spinner-grow spinner-grow-sm\" role=\"status\" aria-hidden=\"true\"></span>\n\n                <span *ngIf=\"!(RTLCalibrateModel?.status || false)\"> PPM Calibrating, waith...</span>\n\n                <span *ngIf=\"!grgsmscannerModel.proc && (RTLCalibrateModel?.status || false)\">Scan BTS's</span>\n\n                <span *ngIf=\"grgsmscannerModel.proc\"> Scanning BTS's...</span>\n              </button>\n\n              <button\n                [hidden]=\"!grgsmscannerModel.proc\"\n                class=\"col btn btn-outline-danger mx-1\"\n                type=\"button\"\n                (click)='grgsmScannerAction($event,0)'>\n                <span *ngIf=\"grgsmscannerModel.action\">Cancel scan BTS's</span>\n                <span *ngIf=\"!grgsmscannerModel.action\" class=\"spinner-grow spinner-grow-sm\" role=\"status\" aria-hidden=\"true\"></span>\n                <span *ngIf=\"!grgsmscannerModel.action\"> Canceling scan BTS's...</span>\n              </button>\n            </div>\n          </div>\n        </div>\n\n        <div #btsTableSection class=\"border rounded p-2 mt-3 mx-3\" style=\"height: 100vh;\" [ngClass]=\"{ 'align-items-center': !withInternet }\">\n          <as-split\n            direction=\"horizontal\">\n\n            <as-split-area [size]=\"60\" class=\"tableBtsSection px-1\">\n\n              <as-split\n                direction=\"vertical\"\n                restrictMove=\"true\"\n                (dragEnd)='verticalTablesDragSplit($event)'\n                >\n\n                <as-split-area [size]='45' class=\"btsTableScroll\">\n                  <div class=\"row align-items-end justify-content-between m-0\">\n                    <h2 class=\"col-*\">BTS's</h2>\n                    <h6 *ngIf=\"grgsmscannerModel.model != null && (grgsmscannerModel.model?.data?.data?.length || 0) > 0 && this.grgsmscannerModel.model.data?.config?.ppm != null\" class=\"text-warning\" ><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> Scanned with [ GAIN: {{ this.grgsmscannerModel.model.data.config.gain }} | SPEED: {{ this.grgsmscannerModel.model.data.config.speed }} | PPM: {{ this.grgsmscannerModel.model.data.config.ppm }} ] </h6>\n                  </div>\n\n                  <table class=\"table\">\n                    <thead class=\"thead-dark w-100\">\n                      <tr>\n                        <th scope=\"col\">#</th>\n                        <th scope=\"col\">ARFCN</th>\n                        <th scope=\"col\">CID</th>\n                        <th scope=\"col\">FREQ</th>\n                        <th scope=\"col\">LAC</th>\n                        <th scope=\"col\">MCC</th>\n                        <th scope=\"col\">MNC</th>\n                        <th scope=\"col\">PWR</th>\n                        <th scope=\"col\">CORDS</th>\n                        <th scope=\"col\" style=\"width: inherit;\">&nbsp;</th>\n                      </tr>\n                    </thead>\n                    <tbody class=\"text-white\">\n                      <tr *ngIf=\"(grgsmscannerModel.model == null || (grgsmscannerModel.model?.data.status || false) == false) \" class=\"text-center\" style=\"width: inherit;\">\n\n                        <td *ngIf=\"grgsmscannerModel.model == null\" scope=\"row\" colspan=\"10\">NOT BTS's</td>\n\n                        <td *ngIf=\"grgsmscannerModel.model != null && (grgsmscannerModel.model?.data?.data?.length || 0) == 0\" scope=\"row\" colspan=\"10\">BTS's NOT FOUND</td>\n\n                      </tr>\n\n                      <tr *ngIf=\"rtlDeviceModel?.inUse && (rtlDeviceModel?.service | checkProcIsRunning:'grgsm_livemon') == true\" class=\"text-center \" style=\"width: inherit;\">\n                        <td *ngIf=\"!grgsmscannerModel.model && !grgsmscannerModel.model?.data?.data\" scope=\"row\" colspan=\"10\" class=\"text-warning\">BTS's scanning in another location... <i class=\"fa fa-cog fa-spin fa-fw\"></i></td>\n                      </tr>\n\n                      <tr #lineTr [id]='\"btsId_\" + bts.id' *ngFor=\"let bts of (grgsmscannerModel.model ? (grgsmscannerModel.model.data.data ) : []) | filterMNC: 20\" >\n                        <td scope=\"row\">{{ bts.id }}</td>\n                        <td>{{ bts.ARFCN }}</td>\n                        <td>{{ bts.CID }}</td>\n                        <td>{{ bts.Freq }}</td>\n                        <td>{{ bts.LAC }}</td>\n                        <td>{{ bts.MCC }}</td>\n                        <td>{{ bts.MNC }}</td>\n                        <td>{{ bts.Pwr}}</td>\n                        <td>{{ bts.cords ? ('lat: ' + bts.cords.lat + ' | lng: ' + bts.cords.lng) : '-' }}</td>\n                        <td >\n                          <div class=\"btn-group\" role=\"group\">\n                            <button *ngIf=\"bts.cords && withInternet\" #centarMap type=\"button\" class=\"btnCenterMap btn btn-sm btn-outline-info\" (click)='mapCenter(bts.cords,18)' title=\"Center in map\">\n                              <i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>\n                            </button>\n                            <button #btnScannDo type=\"button\" class=\"btnScannDo btn btn-sm btn-outline-success\" (click)='grgsmLivemonAction(btnScannDo,bts.id,bts.Freq,1,btnScannCancel,lineTr, bts)' [disabled]=\"inAction || rtlInUse || !statusModel.model.data.radios.status\">\n                              Scan\n                            </button>\n                            <button #btnScannCancel type=\"button\" class=\"btnScannCancel btn btn-sm btn-outline-danger d-none\" (click)='grgsmLivemonAction(btnScannCancel,0,\"\",0,btnScannDo,lineTr, null)'>\n                              Stop\n                            </button>\n                          </div>\n                        </td>\n                      </tr>\n                    </tbody>\n                  </table>\n                </as-split-area>\n\n                <as-split-area [size]='55' [minSize]=\"55\" *ngIf=\"tsharkModel.model\" >\n                  <h2>TARGETS</h2>\n                  <table class=\"table tableTargets targetsTableScroll\">\n                    <thead class=\"thead-dark\">\n                      <tr>\n                        <th scope=\"col\">#</th>\n                        <th scope=\"col\">DATE</th>\n                        <th scope=\"col\">TIME</th>\n                        <th scope=\"col\">FRAME</th>\n                        <th scope=\"col\">ARFCN</th>\n                        <th scope=\"col\">IMSI</th>\n                        <!--<th scope=\"col\">TMSI</th>-->\n                        <th scope=\"col\">SIGNAL</th>\n                        <th scope=\"col\">SNR</th>\n                        <th scope=\"col\">MCC</th>\n                        <th scope=\"col\">MNC</th>\n                        <th scope=\"col\">LAC</th>\n                        <th scope=\"col\">CID</th>\n                        <th scope=\"col\">DISTANCE</th>\n                        <!--\n                        <th scope=\"col\">CORDS</th>\n                        <th scope=\"col\">&nbsp;</th>\n                        -->\n                      </tr>\n                    </thead>\n                    <tbody class=\"text-white\">\n                      <tr *ngIf=\"!tsharkModel.model || tsharkModel.model?.status == false\" class=\"text-center\">\n                        <td *ngIf=\"!tsharkModel.model && !tsharkModel.model?.data\" scope=\"row\" colspan=\"16\">NOT TARGETS</td>\n                        <td *ngIf=\"tsharkModel.model && (!tsharkModel.model?.data || []).length\" scope=\"row\" colspan=\"16\">TARGETS NOT FOUND</td>\n                        <td *ngIf=\"tsharkModel.model && !tsharkModel.model.status\" scope=\"row\" colspan=\"16\" class=\"h1 text-danger\"><i class=\"fa fa-exclamation-triangle animate__animated animate__infinite animate__pulse\" aria-hidden=\"true\"></i> {{ tsharkModel.model.message }}</td>\n                      </tr>\n                      <tr *ngFor=\"let target of (tsharkModel.model ? tsharkModel.model.data : [])\" >\n                        <td scope=\"row\">{{ target.id }}</td>\n                        <td>{{ target.date }}</td>\n                        <td>{{ target.time }}</td>\n                        <td>{{ target.frame }}</td>\n                        <td>{{ target.arfcn }}</td>\n                        <td>{{ target.imsi}}</td>\n                        <!--<td>{{ target.tmsi}}</td>-->\n                        <td>{{ target.signal }}</td>\n                        <td>{{ target.snr }}</td>\n                        <td>{{ target.mcc }}</td>\n                        <td>{{ target.mnc }}</td>\n                        <td>{{ target.lac }}</td>\n                        <td>{{ target.cid }}</td>\n                        <td>{{ target.dist}}</td>\n                        <!--\n                        <td>{{ target.cords ? ('lat: ' + target.cords.lat + ' | lng: ' + target.cords.lng) : '-' }}</td>\n                        <td>&nbsp;</td>\n                        -->\n                      </tr>\n                    </tbody>\n                  </table>\n\n\n                </as-split-area>\n\n              </as-split>\n\n            </as-split-area>\n\n            <as-split-area [size]=\"40\" class=\"btsMapzone px-1\">\n              <agm-map *ngIf=\"withInternet\" class=\"\"\n                [latitude]=\"mapModel.mapCenterCors.lat\"\n                [longitude]=\"mapModel.mapCenterCors.lng\"\n                [zoom]=\"mapModel.zoom\"\n                (zoomChange)='mapZoomChange($event)'\n                (centerChange)='mapCenterChange($event)'>\n\n                <!-- MARKER: current position -->\n                <agm-marker\n                  [latitude]=\"mapModel.currentCords.lat\"\n                  [longitude]=\"mapModel.currentCords.lng\"\n                  [iconUrl]=\"'assets/images/pins/position64x64.png'\"\n                  [markerDraggable]='true'>\n                </agm-marker>\n                <!-- /MARKER: current position -->\n\n                <!-- MARKER: BTS's -->\n                <ng-container *ngFor=\"let bts of (grgsmscannerModel.model ? (grgsmscannerModel.model.data.data ) : []) | filterMNC: 20 | filterBtsScanning: grgsmscannerModel.scanning; let i = index\">\n                  <agm-marker\n                    *ngIf=\"bts.cords\"\n                    [latitude]=\"bts.cords.lat\"\n                    [longitude]=\"bts.cords.lng\"\n                    [markerDraggable]=\"false\"\n                    [iconUrl]=\"'assets/images/pins/bts_2_64x64.png'\">\n\n                    <agm-info-window>\n                      <div class=\"text-dark text-left\">\n                        <h5 class=\"mb-3\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> BTS Information</h5>\n                        <div class=\"row m-0\">\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">ARFCN</h6>\n                            <p>{{bts.ARFCN}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CID</h6>\n                            <p>{{bts.CID}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">FREQ</h6>\n                            <p>{{bts.Freq}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">LAC</h6>\n                            <p>{{bts.LAC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MCC</h6>\n                            <p>{{bts.MCC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MNC</h6>\n                            <p>{{bts.MNC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">PWR</h6>\n                            <p>{{bts.Pwr}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CORDS</h6>\n                            <p>{{ bts.cords.lat + ' | lng: ' + bts.cords.lng }}</p>\n                          </div>\n                        </div>\n                      </div>\n                    </agm-info-window>\n                  </agm-marker>\n                </ng-container>\n                <!-- /MARKER: BTS's -->\n\n                <!-- MARKER: BTS's SCANNING -->\n                <ng-container *ngIf=\"grgsmscannerModel.scanning != null\" >\n                  <agm-marker\n                    *ngIf=\"grgsmscannerModel.scanning.cords\"\n                    [latitude]=\"grgsmscannerModel.scanning.cords.lat\"\n                    [longitude]=\"grgsmscannerModel.scanning.cords.lng\"\n                    [markerDraggable]=\"false\"\n                    [animation]='\"BOUNCE\"'\n                    [iconUrl]=\"'assets/images/pins/bts_2_64x64_scanning.png'\">\n\n                    <agm-info-window>\n                      <div class=\"text-dark text-left\">\n                        <h5 class=\"mb-3\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> BTS Information</h5>\n                        <div class=\"row m-0\">\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">ARFCN</h6>\n                            <p>{{grgsmscannerModel.scanning.ARFCN}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CID</h6>\n                            <p>{{grgsmscannerModel.scanning.CID}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">FREQ</h6>\n                            <p>{{grgsmscannerModel.scanning.Freq}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">LAC</h6>\n                            <p>{{grgsmscannerModel.scanning.LAC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MCC</h6>\n                            <p>{{grgsmscannerModel.scanning.MCC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MNC</h6>\n                            <p>{{grgsmscannerModel.scanning.MNC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">PWR</h6>\n                            <p>{{grgsmscannerModel.scanning.Pwr}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CORDS</h6>\n                            <p>{{ grgsmscannerModel.scanning.cords.lat + ' | lng: ' + grgsmscannerModel.scanning.cords.lng }}</p>\n                          </div>\n                        </div>\n                      </div>\n                    </agm-info-window>\n                  </agm-marker>\n                </ng-container>\n                <!-- /MARKER: BTS's SCANNING-->\n\n\n                <!-- MARKER: TARGETS -->\n                <ng-container *ngFor=\"let target of (tsharkModel.model ? tsharkModel.model.data : []); let i = index\">\n                  <agm-marker\n                    *ngIf=\"target.cords\"\n                    [latitude]=\"target.cords.lat\"\n                    [longitude]=\"target.cords.lng\"\n                    [markerDraggable]=\"false\"\n                    [iconUrl]=\"'assets/images/pins/cell24x24.png'\">\n\n                    <agm-info-window>\n                      <div class=\"text-dark text-left\">\n                        <h5 class=\"mb-3\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> Target information</h5>\n                        <div class=\"row m-0\">\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">ARFCN</h6>\n                            <p>{{target.arfcn}}</p>\n                          </div>\n                          <!--\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CID</h6>\n                            <p>{{target.CID}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">FREQ</h6>\n                            <p>{{target.Freq}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">LAC</h6>\n                            <p>{{target.LAC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MCC</h6>\n                            <p>{{target.MCC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">MNC</h6>\n                            <p>{{target.MNC}}</p>\n                          </div>\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">PWR</h6>\n                            <p>{{target.Pwr}}</p>\n                          </div>\n                          -->\n                          <div class=\"col-6 col-lg-4\">\n                            <h6 class=\"border-bottom\">CORDS</h6>\n                            <p>{{ target.cords.lat + ' | lng: ' + target.cords.lng }}</p>\n                          </div>\n                        </div>\n                      </div>\n                    </agm-info-window>\n                  </agm-marker>\n                </ng-container>\n                <!-- /MARKER: TARGETS -->\n              </agm-map>\n\n              <div *ngIf=\"!withInternet\" class=\"row align-items-center align-self-center h-100 text-center\" >\n                <h5 class=\"col-* text-uppercase text-info\">\n                  <i class=\"fa fa-map fa-4x d-block mb-2\" aria-hidden=\"true\"></i>\n                  The map cannot be displayed because there is no internet connection\n                </h5>\n              </div>\n\n            </as-split-area>\n\n          </as-split>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n</div>\n\n<!--\n<div style=\"width: 100%$; height: 500px;\">\n  <as-split direction=\"horizontal\">\n    <as-split-area [size]=\"40\">\n      <p>LEFT</p>\n    </as-split-area>\n    <as-split-area [size]=\"60\">\n      <p>RIGHT</p>\n    </as-split-area>\n  </as-split>\n</div>\n-->\n");

/***/ }),

/***/ "cWDR":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shared/footer/footer.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<div class=\"bg-dark\" [ngStyle]=\"{ 'min-height':'8px'  }\" ></div>-->\n");

/***/ }),

/***/ "hNQr":
/*!*********************************************************!*\
  !*** ./src/app/pages/shared/footer/footer.component.ts ***!
  \*********************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_footer_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./footer.component.html */ "cWDR");
/* harmony import */ var _footer_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer.component.scss */ "FGth");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");




let FooterComponent = class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
};
FooterComponent.ctorParameters = () => [];
FooterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-footer',
        template: _raw_loader_footer_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_footer_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], FooterComponent);



/***/ }),

/***/ "sUGB":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shared/header/header.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"projectVersions\" class=\"bg-dark text-right px-2\" [ngStyle]=\"{ 'min-height':'8px'  }\" >\n  <i class=\"fa fa-code-fork\" aria-hidden=\"true\"></i> [ imsi <strong>{{projectVersions.imsi}}</strong> | ws <strong>{{projectVersions.ws}}</strong> ]\n</div>\n");

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _pages_shared_only_one_instance_only_one_instance_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/shared/only-one-instance/only-one-instance.component */ "18sH");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _pages_main_main_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/main/main.component */ "/s1f");
/* harmony import */ var _pages_shared_errs_err404_err404_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/shared/errs/err404/err404.component */ "AXJl");






const routes = [
    { path: '', component: _pages_main_main_component__WEBPACK_IMPORTED_MODULE_4__["MainComponent"] },
    { path: 'main', redirectTo: '' },
    { path: 'singleInstance', component: _pages_shared_only_one_instance_only_one_instance_component__WEBPACK_IMPORTED_MODULE_1__["OnlyOneInstanceComponent"] },
    { path: '**', component: _pages_shared_errs_err404_err404_component__WEBPACK_IMPORTED_MODULE_5__["Err404Component"], data: { title: 'Página no encontrada' } }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(routes, { onSameUrlNavigation: 'reload' })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "ygYd":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/shared/only-one-instance/only-one-instance.component.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"h-100\">\n  <div class=\"row align-items-center h-100 m-0\">\n      <div class=\"col-* mx-auto text-center\">\n        <h1 class=\"text-warning animate__animated animate__infinite animate__pulse\"><i class=\"fa fa-exclamation-triangle fa-4x\" aria-hidden=\"true\"></i></h1>\n        <h1 class=\"text-uppercase\">only one instance of the IMSI application is supported per browser</h1>\n      </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "yopv":
/*!*****************************************************!*\
  !*** ./src/app/pipes/check-proc-is-running.pipe.ts ***!
  \*****************************************************/
/*! exports provided: CheckProcIsRunningPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckProcIsRunningPipe", function() { return CheckProcIsRunningPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");


let CheckProcIsRunningPipe = class CheckProcIsRunningPipe {
    transform(value, procName) {
        if (Array.isArray(value)) {
            return value.find((qry) => qry.service == procName) != null ? true : false;
        }
        return false;
    }
};
CheckProcIsRunningPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'checkProcIsRunning'
    })
], CheckProcIsRunningPipe);



/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "wAiw");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map